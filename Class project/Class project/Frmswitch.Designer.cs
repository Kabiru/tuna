﻿namespace Class_project
{
    partial class Frmswitch
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.TxtEname = new System.Windows.Forms.TextBox();
            this.TxtEnumber = new System.Windows.Forms.TextBox();
            this.CboJobGrade = new System.Windows.Forms.ComboBox();
            this.DgvEmployee = new System.Windows.Forms.DataGridView();
            this.Column1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column5 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column7 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column8 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.BtnView = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.DgvEmployee)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(13, 13);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(82, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Employee name";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(13, 36);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(91, 13);
            this.label2.TabIndex = 1;
            this.label2.Text = "Employee number";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(13, 66);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(87, 13);
            this.label3.TabIndex = 2;
            this.label3.Text = "Select Job grade";
            // 
            // TxtEname
            // 
            this.TxtEname.Location = new System.Drawing.Point(113, 13);
            this.TxtEname.Name = "TxtEname";
            this.TxtEname.Size = new System.Drawing.Size(100, 20);
            this.TxtEname.TabIndex = 3;
            // 
            // TxtEnumber
            // 
            this.TxtEnumber.Location = new System.Drawing.Point(110, 40);
            this.TxtEnumber.Name = "TxtEnumber";
            this.TxtEnumber.Size = new System.Drawing.Size(100, 20);
            this.TxtEnumber.TabIndex = 4;
            // 
            // CboJobGrade
            // 
            this.CboJobGrade.FormattingEnabled = true;
            this.CboJobGrade.Items.AddRange(new object[] {
            "M-1",
            "M-4",
            "S-4",
            "S-9"});
            this.CboJobGrade.Location = new System.Drawing.Point(113, 66);
            this.CboJobGrade.Name = "CboJobGrade";
            this.CboJobGrade.Size = new System.Drawing.Size(100, 21);
            this.CboJobGrade.TabIndex = 5;
            // 
            // DgvEmployee
            // 
            this.DgvEmployee.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.DgvEmployee.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Column1,
            this.Column2,
            this.Column3,
            this.Column4,
            this.Column5,
            this.Column7,
            this.Column8});
            this.DgvEmployee.Location = new System.Drawing.Point(16, 179);
            this.DgvEmployee.Name = "DgvEmployee";
            this.DgvEmployee.Size = new System.Drawing.Size(721, 150);
            this.DgvEmployee.TabIndex = 6;
            // 
            // Column1
            // 
            this.Column1.HeaderText = "Employee number";
            this.Column1.Name = "Column1";
            // 
            // Column2
            // 
            this.Column2.HeaderText = "Employee name";
            this.Column2.Name = "Column2";
            // 
            // Column3
            // 
            this.Column3.HeaderText = "Job Grade";
            this.Column3.Name = "Column3";
            // 
            // Column4
            // 
            this.Column4.HeaderText = "Basic";
            this.Column4.Name = "Column4";
            // 
            // Column5
            // 
            this.Column5.HeaderText = "Transport";
            this.Column5.Name = "Column5";
            // 
            // Column7
            // 
            this.Column7.HeaderText = "Housing";
            this.Column7.Name = "Column7";
            // 
            // Column8
            // 
            this.Column8.HeaderText = "Gross";
            this.Column8.Name = "Column8";
            // 
            // BtnView
            // 
            this.BtnView.Location = new System.Drawing.Point(148, 118);
            this.BtnView.Name = "BtnView";
            this.BtnView.Size = new System.Drawing.Size(75, 23);
            this.BtnView.TabIndex = 7;
            this.BtnView.Text = "&Employee salary";
            this.BtnView.UseVisualStyleBackColor = true;
            this.BtnView.Click += new System.EventHandler(this.BtnView_Click);
            // 
            // Frmswitch
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(738, 353);
            this.Controls.Add(this.BtnView);
            this.Controls.Add(this.DgvEmployee);
            this.Controls.Add(this.CboJobGrade);
            this.Controls.Add(this.TxtEnumber);
            this.Controls.Add(this.TxtEname);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Name = "Frmswitch";
            this.Text = "Frmswitch";
            ((System.ComponentModel.ISupportInitialize)(this.DgvEmployee)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox TxtEname;
        private System.Windows.Forms.TextBox TxtEnumber;
        private System.Windows.Forms.ComboBox CboJobGrade;
        private System.Windows.Forms.DataGridView DgvEmployee;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column1;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column2;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column3;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column4;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column5;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column7;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column8;
        private System.Windows.Forms.Button BtnView;
    }
}