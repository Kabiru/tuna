﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Class_project
{
    public partial class Frmnestedif : Form
    {
        public Frmnestedif()
        {
            InitializeComponent();
        }

        private void BtnGrade_Click(object sender, EventArgs e)
        {
            if (TxtRegno.Text.Length == 0)
            {
                errorProvider1.SetError(TxtRegno, "registration numbber is required");
                return;
            }
            else
            {
                errorProvider1.SetError(TxtRegno, null);
            }
            int catone, cattwo, finalexam, average;
            char grade = ' ';
            string comment = " ";
            catone = Convert.ToInt32(txtCatone.Text);
            cattwo = Convert.ToInt32(TxtCatwo.Text);
            finalexam = Convert.ToInt32(TxtFinalexam.Text);
            average = catone + cattwo + finalexam;

            if (average >= 70 && average <= 100)
            {
                grade = 'A';
                comment = "Excellent";
            }
            else   if (average >= 60 && average <= 69)
            {
                grade = 'B';
                comment = "Good";
            }
            else if (average >= 50 && average <= 59)
            {
                grade = 'C';
                comment = "Fair";
            }
            else  if (average >= 40 && average <= 49)
            {
                grade = 'D';
                comment = "poor";
            }
            else if (average >= 0 && average <= 39)
            {
                grade = 'F';
                comment = "fail";
            }
            else
            {
                MessageBox.Show("Invalid marks");
                return;//stop the execution of the program
            }
            dgvstudent.Rows.Add(TxtRegno.Text, txtSname.Text, TxtUcode.Text, txtuname.Text, catone, cattwo, finalexam, average, grade, comment);
        }

        private void txtCatone_TextChanged(object sender, EventArgs e)
        {

        }
    }
}
