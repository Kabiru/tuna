﻿namespace Class_project
{
    partial class frmStudentCampus
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage1PD = new System.Windows.Forms.TabPage();
            this.btnCancelPD = new System.Windows.Forms.Button();
            this.btnClearPD = new System.Windows.Forms.Button();
            this.btnNextPD = new System.Windows.Forms.Button();
            this.dateTimePicker1 = new System.Windows.Forms.DateTimePicker();
            this.lblDateofBirth = new System.Windows.Forms.Label();
            this.cboGender = new System.Windows.Forms.ComboBox();
            this.lblGender = new System.Windows.Forms.Label();
            this.txtRegNo = new System.Windows.Forms.TextBox();
            this.lblRegNo = new System.Windows.Forms.Label();
            this.txtLName = new System.Windows.Forms.TextBox();
            this.lblLName = new System.Windows.Forms.Label();
            this.txtFName = new System.Windows.Forms.TextBox();
            this.lblFName = new System.Windows.Forms.Label();
            this.tabPage2CD = new System.Windows.Forms.TabPage();
            this.cboUnit = new System.Windows.Forms.ComboBox();
            this.lblUnit = new System.Windows.Forms.Label();
            this.cboCourse = new System.Windows.Forms.ComboBox();
            this.lblCourse = new System.Windows.Forms.Label();
            this.btnPreviousCD = new System.Windows.Forms.Button();
            this.btnCancelCD = new System.Windows.Forms.Button();
            this.btnClearCD = new System.Windows.Forms.Button();
            this.btnSubmitCD = new System.Windows.Forms.Button();
            this.txtExam = new System.Windows.Forms.TextBox();
            this.txtCat2 = new System.Windows.Forms.TextBox();
            this.txtCat1 = new System.Windows.Forms.TextBox();
            this.lblExam = new System.Windows.Forms.Label();
            this.lblCat2 = new System.Windows.Forms.Label();
            this.lblCat1 = new System.Windows.Forms.Label();
            this.rdoYear4 = new System.Windows.Forms.RadioButton();
            this.rdoYear3 = new System.Windows.Forms.RadioButton();
            this.rdoYear2 = new System.Windows.Forms.RadioButton();
            this.rdoYear1 = new System.Windows.Forms.RadioButton();
            this.lblYear = new System.Windows.Forms.Label();
            this.tabPage3AP = new System.Windows.Forms.TabPage();
            this.btnCloseAP = new System.Windows.Forms.Button();
            this.btnFinishAP = new System.Windows.Forms.Button();
            this.btnPreviousAP = new System.Windows.Forms.Button();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.Column5 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column6 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.errorProvider1 = new System.Windows.Forms.ErrorProvider(this.components);
            this.tabControl1.SuspendLayout();
            this.tabPage1PD.SuspendLayout();
            this.tabPage2CD.SuspendLayout();
            this.tabPage3AP.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).BeginInit();
            this.SuspendLayout();
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPage1PD);
            this.tabControl1.Controls.Add(this.tabPage2CD);
            this.tabControl1.Controls.Add(this.tabPage3AP);
            this.tabControl1.Location = new System.Drawing.Point(24, 12);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(679, 318);
            this.tabControl1.TabIndex = 0;
            // 
            // tabPage1PD
            // 
            this.tabPage1PD.Controls.Add(this.btnCancelPD);
            this.tabPage1PD.Controls.Add(this.btnClearPD);
            this.tabPage1PD.Controls.Add(this.btnNextPD);
            this.tabPage1PD.Controls.Add(this.dateTimePicker1);
            this.tabPage1PD.Controls.Add(this.lblDateofBirth);
            this.tabPage1PD.Controls.Add(this.cboGender);
            this.tabPage1PD.Controls.Add(this.lblGender);
            this.tabPage1PD.Controls.Add(this.txtRegNo);
            this.tabPage1PD.Controls.Add(this.lblRegNo);
            this.tabPage1PD.Controls.Add(this.txtLName);
            this.tabPage1PD.Controls.Add(this.lblLName);
            this.tabPage1PD.Controls.Add(this.txtFName);
            this.tabPage1PD.Controls.Add(this.lblFName);
            this.tabPage1PD.Location = new System.Drawing.Point(4, 22);
            this.tabPage1PD.Name = "tabPage1PD";
            this.tabPage1PD.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1PD.Size = new System.Drawing.Size(671, 292);
            this.tabPage1PD.TabIndex = 0;
            this.tabPage1PD.Text = "Personal Details";
            this.tabPage1PD.UseVisualStyleBackColor = true;
            // 
            // btnCancelPD
            // 
            this.btnCancelPD.Location = new System.Drawing.Point(291, 212);
            this.btnCancelPD.Name = "btnCancelPD";
            this.btnCancelPD.Size = new System.Drawing.Size(75, 23);
            this.btnCancelPD.TabIndex = 12;
            this.btnCancelPD.Text = "&Cancel";
            this.btnCancelPD.UseVisualStyleBackColor = true;
            this.btnCancelPD.Click += new System.EventHandler(this.btnCancelPD_Click);
            // 
            // btnClearPD
            // 
            this.btnClearPD.Location = new System.Drawing.Point(128, 212);
            this.btnClearPD.Name = "btnClearPD";
            this.btnClearPD.Size = new System.Drawing.Size(75, 23);
            this.btnClearPD.TabIndex = 11;
            this.btnClearPD.Text = "Cl&ear";
            this.btnClearPD.UseVisualStyleBackColor = true;
            this.btnClearPD.Click += new System.EventHandler(this.button2_Click);
            // 
            // btnNextPD
            // 
            this.btnNextPD.Location = new System.Drawing.Point(417, 212);
            this.btnNextPD.Name = "btnNextPD";
            this.btnNextPD.Size = new System.Drawing.Size(75, 23);
            this.btnNextPD.TabIndex = 10;
            this.btnNextPD.Text = "&Next";
            this.btnNextPD.UseVisualStyleBackColor = true;
            this.btnNextPD.Click += new System.EventHandler(this.button1_Click);
            // 
            // dateTimePicker1
            // 
            this.dateTimePicker1.Location = new System.Drawing.Point(104, 114);
            this.dateTimePicker1.Name = "dateTimePicker1";
            this.dateTimePicker1.Size = new System.Drawing.Size(200, 20);
            this.dateTimePicker1.TabIndex = 9;
            // 
            // lblDateofBirth
            // 
            this.lblDateofBirth.AutoSize = true;
            this.lblDateofBirth.Location = new System.Drawing.Point(30, 114);
            this.lblDateofBirth.Name = "lblDateofBirth";
            this.lblDateofBirth.Size = new System.Drawing.Size(66, 13);
            this.lblDateofBirth.TabIndex = 8;
            this.lblDateofBirth.Text = "Date of Birth";
            // 
            // cboGender
            // 
            this.cboGender.FormattingEnabled = true;
            this.cboGender.Location = new System.Drawing.Point(393, 69);
            this.cboGender.Name = "cboGender";
            this.cboGender.Size = new System.Drawing.Size(121, 21);
            this.cboGender.TabIndex = 7;
            this.cboGender.SelectedIndexChanged += new System.EventHandler(this.cboGender_SelectedIndexChanged);
            // 
            // lblGender
            // 
            this.lblGender.AutoSize = true;
            this.lblGender.Location = new System.Drawing.Point(325, 69);
            this.lblGender.Name = "lblGender";
            this.lblGender.Size = new System.Drawing.Size(42, 13);
            this.lblGender.TabIndex = 6;
            this.lblGender.Text = "Gender";
            // 
            // txtRegNo
            // 
            this.txtRegNo.Location = new System.Drawing.Point(393, 28);
            this.txtRegNo.Name = "txtRegNo";
            this.txtRegNo.Size = new System.Drawing.Size(100, 20);
            this.txtRegNo.TabIndex = 5;
            // 
            // lblRegNo
            // 
            this.lblRegNo.AutoSize = true;
            this.lblRegNo.Location = new System.Drawing.Point(322, 31);
            this.lblRegNo.Name = "lblRegNo";
            this.lblRegNo.Size = new System.Drawing.Size(44, 13);
            this.lblRegNo.TabIndex = 4;
            this.lblRegNo.Text = "Reg No";
            // 
            // txtLName
            // 
            this.txtLName.Location = new System.Drawing.Point(104, 70);
            this.txtLName.Name = "txtLName";
            this.txtLName.Size = new System.Drawing.Size(100, 20);
            this.txtLName.TabIndex = 3;
            // 
            // lblLName
            // 
            this.lblLName.AutoSize = true;
            this.lblLName.Location = new System.Drawing.Point(26, 72);
            this.lblLName.Name = "lblLName";
            this.lblLName.Size = new System.Drawing.Size(58, 13);
            this.lblLName.TabIndex = 2;
            this.lblLName.Text = "Last Name";
            // 
            // txtFName
            // 
            this.txtFName.Location = new System.Drawing.Point(104, 31);
            this.txtFName.Name = "txtFName";
            this.txtFName.Size = new System.Drawing.Size(100, 20);
            this.txtFName.TabIndex = 1;
            // 
            // lblFName
            // 
            this.lblFName.AutoSize = true;
            this.lblFName.Location = new System.Drawing.Point(27, 31);
            this.lblFName.Name = "lblFName";
            this.lblFName.Size = new System.Drawing.Size(57, 13);
            this.lblFName.TabIndex = 0;
            this.lblFName.Text = "First Name";
            // 
            // tabPage2CD
            // 
            this.tabPage2CD.Controls.Add(this.cboUnit);
            this.tabPage2CD.Controls.Add(this.lblUnit);
            this.tabPage2CD.Controls.Add(this.cboCourse);
            this.tabPage2CD.Controls.Add(this.lblCourse);
            this.tabPage2CD.Controls.Add(this.btnPreviousCD);
            this.tabPage2CD.Controls.Add(this.btnCancelCD);
            this.tabPage2CD.Controls.Add(this.btnClearCD);
            this.tabPage2CD.Controls.Add(this.btnSubmitCD);
            this.tabPage2CD.Controls.Add(this.txtExam);
            this.tabPage2CD.Controls.Add(this.txtCat2);
            this.tabPage2CD.Controls.Add(this.txtCat1);
            this.tabPage2CD.Controls.Add(this.lblExam);
            this.tabPage2CD.Controls.Add(this.lblCat2);
            this.tabPage2CD.Controls.Add(this.lblCat1);
            this.tabPage2CD.Controls.Add(this.rdoYear4);
            this.tabPage2CD.Controls.Add(this.rdoYear3);
            this.tabPage2CD.Controls.Add(this.rdoYear2);
            this.tabPage2CD.Controls.Add(this.rdoYear1);
            this.tabPage2CD.Controls.Add(this.lblYear);
            this.tabPage2CD.Location = new System.Drawing.Point(4, 22);
            this.tabPage2CD.Name = "tabPage2CD";
            this.tabPage2CD.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2CD.Size = new System.Drawing.Size(671, 292);
            this.tabPage2CD.TabIndex = 1;
            this.tabPage2CD.Text = "Course Details";
            this.tabPage2CD.UseVisualStyleBackColor = true;
            this.tabPage2CD.Click += new System.EventHandler(this.tabPage2_Click);
            // 
            // cboUnit
            // 
            this.cboUnit.FormattingEnabled = true;
            this.cboUnit.Location = new System.Drawing.Point(282, 84);
            this.cboUnit.Name = "cboUnit";
            this.cboUnit.Size = new System.Drawing.Size(187, 21);
            this.cboUnit.TabIndex = 21;
            // 
            // lblUnit
            // 
            this.lblUnit.AutoSize = true;
            this.lblUnit.Location = new System.Drawing.Point(231, 83);
            this.lblUnit.Name = "lblUnit";
            this.lblUnit.Size = new System.Drawing.Size(26, 13);
            this.lblUnit.TabIndex = 20;
            this.lblUnit.Text = "Unit";
            // 
            // cboCourse
            // 
            this.cboCourse.FormattingEnabled = true;
            this.cboCourse.Location = new System.Drawing.Point(90, 84);
            this.cboCourse.Name = "cboCourse";
            this.cboCourse.Size = new System.Drawing.Size(100, 21);
            this.cboCourse.TabIndex = 19;
            this.cboCourse.SelectedIndexChanged += new System.EventHandler(this.cboCourse_SelectedIndexChanged);
            // 
            // lblCourse
            // 
            this.lblCourse.AutoSize = true;
            this.lblCourse.Location = new System.Drawing.Point(37, 84);
            this.lblCourse.Name = "lblCourse";
            this.lblCourse.Size = new System.Drawing.Size(40, 13);
            this.lblCourse.TabIndex = 18;
            this.lblCourse.Text = "Course";
            this.lblCourse.Click += new System.EventHandler(this.label1_Click);
            // 
            // btnPreviousCD
            // 
            this.btnPreviousCD.Location = new System.Drawing.Point(332, 214);
            this.btnPreviousCD.Name = "btnPreviousCD";
            this.btnPreviousCD.Size = new System.Drawing.Size(75, 23);
            this.btnPreviousCD.TabIndex = 17;
            this.btnPreviousCD.Text = "&Previous";
            this.btnPreviousCD.UseVisualStyleBackColor = true;
            this.btnPreviousCD.Click += new System.EventHandler(this.btnPreviousCD_Click);
            // 
            // btnCancelCD
            // 
            this.btnCancelCD.Location = new System.Drawing.Point(191, 214);
            this.btnCancelCD.Name = "btnCancelCD";
            this.btnCancelCD.Size = new System.Drawing.Size(75, 23);
            this.btnCancelCD.TabIndex = 16;
            this.btnCancelCD.Text = "&Cancel";
            this.btnCancelCD.UseVisualStyleBackColor = true;
            this.btnCancelCD.Click += new System.EventHandler(this.btnCancelCD_Click);
            // 
            // btnClearCD
            // 
            this.btnClearCD.Location = new System.Drawing.Point(90, 214);
            this.btnClearCD.Name = "btnClearCD";
            this.btnClearCD.Size = new System.Drawing.Size(75, 23);
            this.btnClearCD.TabIndex = 15;
            this.btnClearCD.Text = "Cl&ear";
            this.btnClearCD.UseVisualStyleBackColor = true;
            this.btnClearCD.Click += new System.EventHandler(this.btnClearCD_Click);
            // 
            // btnSubmitCD
            // 
            this.btnSubmitCD.Location = new System.Drawing.Point(442, 214);
            this.btnSubmitCD.Name = "btnSubmitCD";
            this.btnSubmitCD.Size = new System.Drawing.Size(75, 23);
            this.btnSubmitCD.TabIndex = 14;
            this.btnSubmitCD.Text = "&Submit";
            this.btnSubmitCD.UseVisualStyleBackColor = true;
            this.btnSubmitCD.Click += new System.EventHandler(this.btnNextCD_Click);
            // 
            // txtExam
            // 
            this.txtExam.Location = new System.Drawing.Point(90, 167);
            this.txtExam.Name = "txtExam";
            this.txtExam.Size = new System.Drawing.Size(100, 20);
            this.txtExam.TabIndex = 10;
            // 
            // txtCat2
            // 
            this.txtCat2.Location = new System.Drawing.Point(282, 133);
            this.txtCat2.Name = "txtCat2";
            this.txtCat2.Size = new System.Drawing.Size(100, 20);
            this.txtCat2.TabIndex = 9;
            // 
            // txtCat1
            // 
            this.txtCat1.Location = new System.Drawing.Point(90, 133);
            this.txtCat1.Name = "txtCat1";
            this.txtCat1.Size = new System.Drawing.Size(100, 20);
            this.txtCat1.TabIndex = 8;
            this.txtCat1.Validating += new System.ComponentModel.CancelEventHandler(this.txtCat1_Validating);
            // 
            // lblExam
            // 
            this.lblExam.AutoSize = true;
            this.lblExam.Location = new System.Drawing.Point(35, 175);
            this.lblExam.Name = "lblExam";
            this.lblExam.Size = new System.Drawing.Size(37, 13);
            this.lblExam.TabIndex = 7;
            this.lblExam.Text = "EXAM";
            // 
            // lblCat2
            // 
            this.lblCat2.AutoSize = true;
            this.lblCat2.Location = new System.Drawing.Point(228, 133);
            this.lblCat2.Name = "lblCat2";
            this.lblCat2.Size = new System.Drawing.Size(37, 13);
            this.lblCat2.TabIndex = 6;
            this.lblCat2.Text = "CAT 2";
            // 
            // lblCat1
            // 
            this.lblCat1.AutoSize = true;
            this.lblCat1.Location = new System.Drawing.Point(34, 133);
            this.lblCat1.Name = "lblCat1";
            this.lblCat1.Size = new System.Drawing.Size(37, 13);
            this.lblCat1.TabIndex = 5;
            this.lblCat1.Text = "CAT 1";
            // 
            // rdoYear4
            // 
            this.rdoYear4.AutoSize = true;
            this.rdoYear4.Location = new System.Drawing.Point(231, 36);
            this.rdoYear4.Name = "rdoYear4";
            this.rdoYear4.Size = new System.Drawing.Size(35, 17);
            this.rdoYear4.TabIndex = 4;
            this.rdoYear4.TabStop = true;
            this.rdoYear4.Text = "IV";
            this.rdoYear4.UseVisualStyleBackColor = true;
            // 
            // rdoYear3
            // 
            this.rdoYear3.AutoSize = true;
            this.rdoYear3.Location = new System.Drawing.Point(181, 36);
            this.rdoYear3.Name = "rdoYear3";
            this.rdoYear3.Size = new System.Drawing.Size(34, 17);
            this.rdoYear3.TabIndex = 3;
            this.rdoYear3.TabStop = true;
            this.rdoYear3.Text = "III";
            this.rdoYear3.UseVisualStyleBackColor = true;
            // 
            // rdoYear2
            // 
            this.rdoYear2.AutoSize = true;
            this.rdoYear2.Location = new System.Drawing.Point(133, 36);
            this.rdoYear2.Name = "rdoYear2";
            this.rdoYear2.Size = new System.Drawing.Size(31, 17);
            this.rdoYear2.TabIndex = 2;
            this.rdoYear2.TabStop = true;
            this.rdoYear2.Text = "II";
            this.rdoYear2.UseVisualStyleBackColor = true;
            // 
            // rdoYear1
            // 
            this.rdoYear1.AutoSize = true;
            this.rdoYear1.Location = new System.Drawing.Point(90, 36);
            this.rdoYear1.Name = "rdoYear1";
            this.rdoYear1.Size = new System.Drawing.Size(28, 17);
            this.rdoYear1.TabIndex = 1;
            this.rdoYear1.TabStop = true;
            this.rdoYear1.Text = "I";
            this.rdoYear1.UseVisualStyleBackColor = true;
            // 
            // lblYear
            // 
            this.lblYear.AutoSize = true;
            this.lblYear.Location = new System.Drawing.Point(34, 36);
            this.lblYear.Name = "lblYear";
            this.lblYear.Size = new System.Drawing.Size(36, 13);
            this.lblYear.TabIndex = 0;
            this.lblYear.Text = "YEAR";
            // 
            // tabPage3AP
            // 
            this.tabPage3AP.Controls.Add(this.btnCloseAP);
            this.tabPage3AP.Controls.Add(this.btnFinishAP);
            this.tabPage3AP.Controls.Add(this.btnPreviousAP);
            this.tabPage3AP.Controls.Add(this.dataGridView1);
            this.tabPage3AP.Location = new System.Drawing.Point(4, 22);
            this.tabPage3AP.Name = "tabPage3AP";
            this.tabPage3AP.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage3AP.Size = new System.Drawing.Size(671, 292);
            this.tabPage3AP.TabIndex = 2;
            this.tabPage3AP.Text = "Academic Progress";
            this.tabPage3AP.UseVisualStyleBackColor = true;
            // 
            // btnCloseAP
            // 
            this.btnCloseAP.Location = new System.Drawing.Point(238, 249);
            this.btnCloseAP.Name = "btnCloseAP";
            this.btnCloseAP.Size = new System.Drawing.Size(75, 23);
            this.btnCloseAP.TabIndex = 3;
            this.btnCloseAP.Text = "&Close";
            this.btnCloseAP.UseVisualStyleBackColor = true;
            this.btnCloseAP.Click += new System.EventHandler(this.btnCancelAP_Click);
            // 
            // btnFinishAP
            // 
            this.btnFinishAP.Location = new System.Drawing.Point(535, 249);
            this.btnFinishAP.Name = "btnFinishAP";
            this.btnFinishAP.Size = new System.Drawing.Size(75, 23);
            this.btnFinishAP.TabIndex = 2;
            this.btnFinishAP.Text = "&Finish";
            this.btnFinishAP.UseVisualStyleBackColor = true;
            this.btnFinishAP.Click += new System.EventHandler(this.btnFinishAP_Click);
            // 
            // btnPreviousAP
            // 
            this.btnPreviousAP.Location = new System.Drawing.Point(411, 249);
            this.btnPreviousAP.Name = "btnPreviousAP";
            this.btnPreviousAP.Size = new System.Drawing.Size(75, 23);
            this.btnPreviousAP.TabIndex = 1;
            this.btnPreviousAP.Text = "&Previous";
            this.btnPreviousAP.UseVisualStyleBackColor = true;
            this.btnPreviousAP.Click += new System.EventHandler(this.btnPreviousAP_Click);
            // 
            // dataGridView1
            // 
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Column5,
            this.Column1,
            this.Column2,
            this.Column3,
            this.Column4,
            this.Column6});
            this.dataGridView1.Location = new System.Drawing.Point(17, 25);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.Size = new System.Drawing.Size(642, 206);
            this.dataGridView1.TabIndex = 0;
            // 
            // Column5
            // 
            this.Column5.HeaderText = "Reg No";
            this.Column5.Name = "Column5";
            this.Column5.ReadOnly = true;
            // 
            // Column1
            // 
            this.Column1.HeaderText = "Last Name";
            this.Column1.Name = "Column1";
            this.Column1.ReadOnly = true;
            // 
            // Column2
            // 
            this.Column2.HeaderText = "First Name";
            this.Column2.Name = "Column2";
            this.Column2.ReadOnly = true;
            // 
            // Column3
            // 
            this.Column3.HeaderText = "Gender";
            this.Column3.Name = "Column3";
            this.Column3.ReadOnly = true;
            // 
            // Column4
            // 
            this.Column4.HeaderText = "Date of Birth";
            this.Column4.Name = "Column4";
            this.Column4.ReadOnly = true;
            // 
            // Column6
            // 
            this.Column6.HeaderText = "Grade";
            this.Column6.Name = "Column6";
            this.Column6.ReadOnly = true;
            // 
            // errorProvider1
            // 
            this.errorProvider1.ContainerControl = this;
            // 
            // frmStudentCampus
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(715, 342);
            this.Controls.Add(this.tabControl1);
            this.Name = "frmStudentCampus";
            this.Text = "frmStudentCampus";
            this.Load += new System.EventHandler(this.frmStudentCampus_Load);
            this.tabControl1.ResumeLayout(false);
            this.tabPage1PD.ResumeLayout(false);
            this.tabPage1PD.PerformLayout();
            this.tabPage2CD.ResumeLayout(false);
            this.tabPage2CD.PerformLayout();
            this.tabPage3AP.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage1PD;
        private System.Windows.Forms.TabPage tabPage2CD;
        private System.Windows.Forms.Button btnNextPD;
        private System.Windows.Forms.DateTimePicker dateTimePicker1;
        private System.Windows.Forms.Label lblDateofBirth;
        private System.Windows.Forms.ComboBox cboGender;
        private System.Windows.Forms.Label lblGender;
        private System.Windows.Forms.TextBox txtRegNo;
        private System.Windows.Forms.Label lblRegNo;
        private System.Windows.Forms.TextBox txtLName;
        private System.Windows.Forms.Label lblLName;
        private System.Windows.Forms.TextBox txtFName;
        private System.Windows.Forms.Label lblFName;
        private System.Windows.Forms.TabPage tabPage3AP;
        private System.Windows.Forms.Button btnClearPD;
        private System.Windows.Forms.Button btnCancelPD;
        private System.Windows.Forms.Button btnPreviousCD;
        private System.Windows.Forms.Button btnCancelCD;
        private System.Windows.Forms.Button btnClearCD;
        private System.Windows.Forms.Button btnSubmitCD;
        private System.Windows.Forms.TextBox txtExam;
        private System.Windows.Forms.TextBox txtCat2;
        private System.Windows.Forms.TextBox txtCat1;
        private System.Windows.Forms.Label lblExam;
        private System.Windows.Forms.Label lblCat2;
        private System.Windows.Forms.Label lblCat1;
        private System.Windows.Forms.RadioButton rdoYear4;
        private System.Windows.Forms.RadioButton rdoYear3;
        private System.Windows.Forms.RadioButton rdoYear2;
        private System.Windows.Forms.RadioButton rdoYear1;
        private System.Windows.Forms.Label lblYear;
        private System.Windows.Forms.Button btnCloseAP;
        private System.Windows.Forms.Button btnFinishAP;
        private System.Windows.Forms.Button btnPreviousAP;
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.Label lblCourse;
        private System.Windows.Forms.ComboBox cboUnit;
        private System.Windows.Forms.Label lblUnit;
        private System.Windows.Forms.ComboBox cboCourse;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column5;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column1;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column2;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column3;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column4;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column6;
        private System.Windows.Forms.ErrorProvider errorProvider1;
    }
}