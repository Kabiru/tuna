﻿namespace Class_project
{
    partial class Frmif
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.txregno = new System.Windows.Forms.TextBox();
            this.txtucode = new System.Windows.Forms.TextBox();
            this.txtcatone = new System.Windows.Forms.TextBox();
            this.txtcattwo = new System.Windows.Forms.TextBox();
            this.txtfinalexam = new System.Windows.Forms.TextBox();
            this.txtuname = new System.Windows.Forms.TextBox();
            this.txtsname = new System.Windows.Forms.TextBox();
            this.btngrade = new System.Windows.Forms.Button();
            this.dgvgrade = new System.Windows.Forms.DataGridView();
            this.Column1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column5 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column6 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column7 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column8 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column9 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column10 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            ((System.ComponentModel.ISupportInitialize)(this.dgvgrade)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(23, 13);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(101, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Registration number";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(32, 139);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(71, 13);
            this.label2.TabIndex = 1;
            this.label2.Text = "cat one[x/15]";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(32, 110);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(51, 13);
            this.label3.TabIndex = 2;
            this.label3.Text = "unit code";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(32, 81);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(53, 13);
            this.label4.TabIndex = 3;
            this.label4.Text = "unit name";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(23, 49);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(73, 13);
            this.label5.TabIndex = 4;
            this.label5.Text = "Student name";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(32, 167);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(74, 13);
            this.label6.TabIndex = 5;
            this.label6.Text = "cat Two[x/15]";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(23, 200);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(85, 13);
            this.label7.TabIndex = 6;
            this.label7.Text = "Final exam[x/70]";
            // 
            // txregno
            // 
            this.txregno.Location = new System.Drawing.Point(193, 23);
            this.txregno.Name = "txregno";
            this.txregno.Size = new System.Drawing.Size(100, 20);
            this.txregno.TabIndex = 7;
            // 
            // txtucode
            // 
            this.txtucode.Location = new System.Drawing.Point(193, 120);
            this.txtucode.Name = "txtucode";
            this.txtucode.Size = new System.Drawing.Size(100, 20);
            this.txtucode.TabIndex = 8;
            // 
            // txtcatone
            // 
            this.txtcatone.Location = new System.Drawing.Point(193, 146);
            this.txtcatone.Name = "txtcatone";
            this.txtcatone.Size = new System.Drawing.Size(100, 20);
            this.txtcatone.TabIndex = 9;
            // 
            // txtcattwo
            // 
            this.txtcattwo.Location = new System.Drawing.Point(193, 176);
            this.txtcattwo.Name = "txtcattwo";
            this.txtcattwo.Size = new System.Drawing.Size(100, 20);
            this.txtcattwo.TabIndex = 10;
            // 
            // txtfinalexam
            // 
            this.txtfinalexam.Location = new System.Drawing.Point(202, 211);
            this.txtfinalexam.Name = "txtfinalexam";
            this.txtfinalexam.Size = new System.Drawing.Size(100, 20);
            this.txtfinalexam.TabIndex = 11;
            // 
            // txtuname
            // 
            this.txtuname.Location = new System.Drawing.Point(193, 94);
            this.txtuname.Name = "txtuname";
            this.txtuname.Size = new System.Drawing.Size(100, 20);
            this.txtuname.TabIndex = 12;
            // 
            // txtsname
            // 
            this.txtsname.Location = new System.Drawing.Point(193, 68);
            this.txtsname.Name = "txtsname";
            this.txtsname.Size = new System.Drawing.Size(100, 20);
            this.txtsname.TabIndex = 13;
            // 
            // btngrade
            // 
            this.btngrade.Location = new System.Drawing.Point(126, 243);
            this.btngrade.Name = "btngrade";
            this.btngrade.Size = new System.Drawing.Size(176, 23);
            this.btngrade.TabIndex = 14;
            this.btngrade.Text = "&Calculate Grade";
            this.btngrade.UseVisualStyleBackColor = true;
            this.btngrade.Click += new System.EventHandler(this.btngrade_Click);
            // 
            // dgvgrade
            // 
            this.dgvgrade.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvgrade.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Column1,
            this.Column2,
            this.Column3,
            this.Column4,
            this.Column5,
            this.Column6,
            this.Column7,
            this.Column8,
            this.Column9,
            this.Column10});
            this.dgvgrade.Location = new System.Drawing.Point(12, 284);
            this.dgvgrade.Name = "dgvgrade";
            this.dgvgrade.Size = new System.Drawing.Size(881, 150);
            this.dgvgrade.TabIndex = 15;
            this.dgvgrade.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridView1_CellContentClick);
            // 
            // Column1
            // 
            this.Column1.HeaderText = "Regno";
            this.Column1.Name = "Column1";
            // 
            // Column2
            // 
            this.Column2.HeaderText = "name";
            this.Column2.Name = "Column2";
            // 
            // Column3
            // 
            this.Column3.HeaderText = "unit name";
            this.Column3.Name = "Column3";
            // 
            // Column4
            // 
            this.Column4.HeaderText = "Unit Code";
            this.Column4.Name = "Column4";
            // 
            // Column5
            // 
            this.Column5.HeaderText = "cat one ";
            this.Column5.Name = "Column5";
            // 
            // Column6
            // 
            this.Column6.HeaderText = "Cat Two";
            this.Column6.Name = "Column6";
            // 
            // Column7
            // 
            this.Column7.HeaderText = "Exam mark";
            this.Column7.Name = "Column7";
            // 
            // Column8
            // 
            this.Column8.HeaderText = "average mark";
            this.Column8.Name = "Column8";
            // 
            // Column9
            // 
            this.Column9.HeaderText = "Grade";
            this.Column9.Name = "Column9";
            // 
            // Column10
            // 
            this.Column10.HeaderText = "Remarks/Comments";
            this.Column10.Name = "Column10";
            // 
            // Frmif
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(952, 468);
            this.Controls.Add(this.dgvgrade);
            this.Controls.Add(this.btngrade);
            this.Controls.Add(this.txtsname);
            this.Controls.Add(this.txtuname);
            this.Controls.Add(this.txtfinalexam);
            this.Controls.Add(this.txtcattwo);
            this.Controls.Add(this.txtcatone);
            this.Controls.Add(this.txtucode);
            this.Controls.Add(this.txregno);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Name = "Frmif";
            this.Text = "Frmif";
            ((System.ComponentModel.ISupportInitialize)(this.dgvgrade)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox txregno;
        private System.Windows.Forms.TextBox txtucode;
        private System.Windows.Forms.TextBox txtcatone;
        private System.Windows.Forms.TextBox txtcattwo;
        private System.Windows.Forms.TextBox txtfinalexam;
        private System.Windows.Forms.TextBox txtuname;
        private System.Windows.Forms.TextBox txtsname;
        private System.Windows.Forms.Button btngrade;
        private System.Windows.Forms.DataGridView dgvgrade;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column1;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column2;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column3;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column4;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column5;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column6;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column7;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column8;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column9;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column10;
    }
}