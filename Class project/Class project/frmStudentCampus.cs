﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Class_project
{
    public partial class frmStudentCampus : Form
    {
        public frmStudentCampus()
        {
            InitializeComponent();

            tabControl1.TabPages.Remove(tabPage2CD);
            tabControl1.TabPages.Remove(tabPage3AP);//hiding tabs
        }

        private void button1_Click(object sender, EventArgs e)
        {
            String FName, LName, Gender;
            DateTime DOB;
            int RegNO;

            FName = txtFName.Text;
            LName = txtLName.Text;
            Gender = cboGender.Text;
            DOB = dateTimePicker1.Value;
            RegNO = Convert.ToInt32(txtRegNo.Text);
            //capturing user input
            try
            {
                if (txtFName.Text == "")
                {
                    errorProvider1.SetError(txtFName, "First Name is Required");
                    txtFName.Focus();//focussing cursor
                    return;
                }

                else if (txtLName.Text == "")
                {
                    errorProvider1.SetError(txtLName, "Last Name is Required");
                    txtLName.Focus();
                    return;
                }
                else if (txtRegNo.Text == "")
                {
                    errorProvider1.SetError(txtRegNo, "Registration Number is Required");
                    txtRegNo.Focus();
                    return;
                }
                else
                {
                    errorProvider1.SetError(txtFName, null);
                    errorProvider1.SetError(txtLName, null);
                    errorProvider1.SetError(txtRegNo, null);
                }
            }
            catch {
                MessageBox.Show("Please enter data to continue");
                txtFName.Focus();
                return;
            }

            tabControl1.TabPages.Add(tabPage2CD);//showing the second tab
            tabControl1.SelectTab(1);//shift to Course Details
        }

        private void button2_Click(object sender, EventArgs e)
        {
            txtFName.Clear();
            txtLName.Clear();
            txtRegNo.Clear();
            //clearing data entered by user
            txtFName.Focus();
        }

        private void tabPage2_Click(object sender, EventArgs e)
        {

        }

        private void btnPreviousCD_Click(object sender, EventArgs e)
        {
            tabControl1.TabPages.Remove(tabPage2CD);
            tabControl1.SelectTab(0);//back to Personal Details
        }

        private void btnNextCD_Click(object sender, EventArgs e)
        {
                       
            tabControl1.TabPages.Add(tabPage3AP);//showig academic progress tab
            tabControl1.SelectTab(2);//shift to Academic progress
        }

        private void btnPreviousAP_Click(object sender, EventArgs e)
        {
            tabControl1.TabPages.Remove(tabPage3AP);//hiding the third tab
            tabControl1.SelectTab(1);//back to Course Details
        }

        private void btnClearCD_Click(object sender, EventArgs e)
        {
            txtCat1.Clear();
            txtCat2.Clear();
            txtExam.Clear();
            //clearing user input
            txtCat1.Focus();//focussing cursor on cat1
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void btnCancelAP_Click(object sender, EventArgs e)
        {
            this.Close();//closing the form
        }

        private void btnCancelCD_Click(object sender, EventArgs e)
        {
            this.Close();//exit application
        }

        private void btnCancelPD_Click(object sender, EventArgs e)
        {
            this.Close();//exit application
        }

        private void cboGender_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void txtCat1_Validating(object sender, CancelEventArgs e)
        {
            int Cat1Mark;

            if (int.TryParse(txtCat1.Text, out Cat1Mark))
            {
                //marks are numeric
                if ( Cat1Mark > 20) {
                    MessageBox.Show("Cat 1 marks cannot be more than 20");
                    txtCat1.Clear();
                }
                else if (Cat1Mark < 0) {
                    MessageBox.Show("Cat 1 marks cannot be less than 0");
                    txtCat1.Clear();
                }
            }
            else {
                MessageBox.Show("Invalid Marks");
                txtCat1.Clear();
                txtCat1.Focus();
                return;
            }
        }

        private void frmStudentCampus_Load(object sender, EventArgs e)
        {
            cboGender.Items.Add("Male");
            cboGender.Items.Add("Female");
            cboGender.SelectedItem = "Female";
            //Gender picking combo box

            cboCourse.Items.Add("BSc Computer Science");
            cboCourse.Items.Add("BSc Law");
            cboCourse.SelectedItem = "Bsc Computer Science";
            //Course picking combo box
        }

        private void cboCourse_SelectedIndexChanged(object sender, EventArgs e)
        {
            cboUnit.Items.Clear();//clearing units

            if (cboCourse.SelectedItem == "BSc Computer Science")
            {
                cboUnit.Items.Add("CMT 101 Fundumentals of Computing");
                cboUnit.Items.Add("CMT 102 Database Systems");
                cboUnit.SelectedItem = "CMT 102 Database Systems";
                //loading units 
            }
            else {
                cboUnit.Items.Add("LWT 101 Criminal Law");
                cboUnit.Items.Add("LWT 102 Cyber Crime");
                cboUnit.Items.Add("LWT 201 Family Law");
                cboUnit.SelectedItem = "LWT 101 Criminal Law";
                //loading units
            }
        }

        private void btnFinishAP_Click(object sender, EventArgs e)
        {
            this.Close();//closing and exiting application
        }
    }
}
