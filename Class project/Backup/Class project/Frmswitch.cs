﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Class_project
{
    public partial class Frmswitch : Form
    {
        public Frmswitch()
        {
            InitializeComponent();
        }

        private void BtnView_Click(object sender, EventArgs e)
        {
            string jobgrade;
            int basic, trasnport, housing, gross;
            jobgrade = CboJobGrade.Text;
            switch (jobgrade)
            {//start of switch
                case "M-1":
                    basic = 280000;
                    trasnport = 3000;
                    housing = 12000;
                    break;
                case "M-4":
                    basic = 280000;
                    trasnport = 4000;
                    housing = 12000;
                    break;
                case "S-4":
                    basic = 700000;
                    trasnport = 20000;
                    housing = 60000;
                    break;
                case "S-9":
                    basic = 750000;
                    trasnport = 25000;
                    housing = 65000;
                    break;
                default:
                    MessageBox.Show("Invalid job grade");
                    return;
            }//end of switch
            gross = housing + basic + trasnport;
            DgvEmployee.Rows.Add(TxtEnumber.Text, TxtEname.Text, jobgrade, basic, trasnport, housing,gross);
        }
    }
}
