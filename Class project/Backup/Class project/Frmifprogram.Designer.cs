﻿namespace Class_project
{
    partial class Frmifprogram
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.TxtRegno = new System.Windows.Forms.TextBox();
            this.txtSname = new System.Windows.Forms.TextBox();
            this.TxtUcode = new System.Windows.Forms.TextBox();
            this.txtuname = new System.Windows.Forms.TextBox();
            this.TxtCatwo = new System.Windows.Forms.TextBox();
            this.txtCatone = new System.Windows.Forms.TextBox();
            this.TxtFinalexam = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.BtnGrade = new System.Windows.Forms.Button();
            this.dgvstudent = new System.Windows.Forms.DataGridView();
            this.Column1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column5 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column6 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column7 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column8 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column9 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column10 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.errorProvider1 = new System.Windows.Forms.ErrorProvider(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.dgvstudent)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(74, 58);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(96, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "registration number";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(77, 92);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(73, 13);
            this.label2.TabIndex = 1;
            this.label2.Text = "Student name";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(77, 116);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(53, 13);
            this.label3.TabIndex = 2;
            this.label3.Text = "unit name";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(77, 143);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(51, 13);
            this.label4.TabIndex = 3;
            this.label4.Text = "unit code";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(77, 170);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(43, 13);
            this.label5.TabIndex = 4;
            this.label5.Text = "cat one";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(77, 201);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(42, 13);
            this.label6.TabIndex = 5;
            this.label6.Text = "cat two";
            // 
            // TxtRegno
            // 
            this.TxtRegno.Location = new System.Drawing.Point(176, 58);
            this.TxtRegno.Name = "TxtRegno";
            this.TxtRegno.Size = new System.Drawing.Size(100, 20);
            this.TxtRegno.TabIndex = 6;
            // 
            // txtSname
            // 
            this.txtSname.Location = new System.Drawing.Point(176, 89);
            this.txtSname.Name = "txtSname";
            this.txtSname.Size = new System.Drawing.Size(100, 20);
            this.txtSname.TabIndex = 7;
            this.txtSname.TextChanged += new System.EventHandler(this.textBox2_TextChanged);
            // 
            // TxtUcode
            // 
            this.TxtUcode.Location = new System.Drawing.Point(166, 149);
            this.TxtUcode.Name = "TxtUcode";
            this.TxtUcode.Size = new System.Drawing.Size(100, 20);
            this.TxtUcode.TabIndex = 8;
            // 
            // txtuname
            // 
            this.txtuname.Location = new System.Drawing.Point(166, 117);
            this.txtuname.Name = "txtuname";
            this.txtuname.Size = new System.Drawing.Size(100, 20);
            this.txtuname.TabIndex = 9;
            // 
            // TxtCatwo
            // 
            this.TxtCatwo.Location = new System.Drawing.Point(166, 201);
            this.TxtCatwo.Name = "TxtCatwo";
            this.TxtCatwo.Size = new System.Drawing.Size(100, 20);
            this.TxtCatwo.TabIndex = 10;
            // 
            // txtCatone
            // 
            this.txtCatone.Location = new System.Drawing.Point(166, 175);
            this.txtCatone.Name = "txtCatone";
            this.txtCatone.Size = new System.Drawing.Size(100, 20);
            this.txtCatone.TabIndex = 11;
            this.txtCatone.Validating += new System.ComponentModel.CancelEventHandler(this.txtCatone_Validating);
            // 
            // TxtFinalexam
            // 
            this.TxtFinalexam.Location = new System.Drawing.Point(176, 227);
            this.TxtFinalexam.Name = "TxtFinalexam";
            this.TxtFinalexam.Size = new System.Drawing.Size(100, 20);
            this.TxtFinalexam.TabIndex = 13;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(87, 227);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(51, 13);
            this.label7.TabIndex = 12;
            this.label7.Text = "finalexam";
            // 
            // BtnGrade
            // 
            this.BtnGrade.Location = new System.Drawing.Point(74, 269);
            this.BtnGrade.Name = "BtnGrade";
            this.BtnGrade.Size = new System.Drawing.Size(153, 23);
            this.BtnGrade.TabIndex = 14;
            this.BtnGrade.Text = "Calculate Grade";
            this.BtnGrade.UseVisualStyleBackColor = true;
            this.BtnGrade.Click += new System.EventHandler(this.BtnGrade_Click);
            // 
            // dgvstudent
            // 
            this.dgvstudent.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvstudent.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Column1,
            this.Column2,
            this.Column3,
            this.Column4,
            this.Column5,
            this.Column6,
            this.Column7,
            this.Column8,
            this.Column9,
            this.Column10});
            this.dgvstudent.Location = new System.Drawing.Point(12, 298);
            this.dgvstudent.Name = "dgvstudent";
            this.dgvstudent.Size = new System.Drawing.Size(942, 150);
            this.dgvstudent.TabIndex = 15;
            // 
            // Column1
            // 
            this.Column1.HeaderText = "Registration number";
            this.Column1.Name = "Column1";
            // 
            // Column2
            // 
            this.Column2.HeaderText = "Name";
            this.Column2.Name = "Column2";
            // 
            // Column3
            // 
            this.Column3.HeaderText = "Unit Name";
            this.Column3.Name = "Column3";
            // 
            // Column4
            // 
            this.Column4.HeaderText = "Unit code";
            this.Column4.Name = "Column4";
            // 
            // Column5
            // 
            this.Column5.HeaderText = "Cat one";
            this.Column5.Name = "Column5";
            // 
            // Column6
            // 
            this.Column6.HeaderText = "cat two";
            this.Column6.Name = "Column6";
            // 
            // Column7
            // 
            this.Column7.HeaderText = "Exam";
            this.Column7.Name = "Column7";
            // 
            // Column8
            // 
            this.Column8.HeaderText = "Average";
            this.Column8.Name = "Column8";
            // 
            // Column9
            // 
            this.Column9.HeaderText = "Grade";
            this.Column9.Name = "Column9";
            // 
            // Column10
            // 
            this.Column10.HeaderText = "Comments";
            this.Column10.Name = "Column10";
            // 
            // errorProvider1
            // 
            this.errorProvider1.ContainerControl = this;
            // 
            // Frmifprogram
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(957, 491);
            this.Controls.Add(this.dgvstudent);
            this.Controls.Add(this.BtnGrade);
            this.Controls.Add(this.TxtFinalexam);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.txtCatone);
            this.Controls.Add(this.TxtCatwo);
            this.Controls.Add(this.txtuname);
            this.Controls.Add(this.TxtUcode);
            this.Controls.Add(this.txtSname);
            this.Controls.Add(this.TxtRegno);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Name = "Frmifprogram";
            this.Text = "Frmifprogram";
            ((System.ComponentModel.ISupportInitialize)(this.dgvstudent)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox TxtRegno;
        private System.Windows.Forms.TextBox txtSname;
        private System.Windows.Forms.TextBox TxtUcode;
        private System.Windows.Forms.TextBox txtuname;
        private System.Windows.Forms.TextBox TxtCatwo;
        private System.Windows.Forms.TextBox txtCatone;
        private System.Windows.Forms.TextBox TxtFinalexam;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Button BtnGrade;
        private System.Windows.Forms.DataGridView dgvstudent;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column1;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column2;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column3;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column4;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column5;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column6;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column7;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column8;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column9;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column10;
        private System.Windows.Forms.ErrorProvider errorProvider1;
    }
}