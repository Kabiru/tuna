﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Class_project
{
    public partial class frmsplash : Form
    {
        public frmsplash()
        {
            InitializeComponent();
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            timer1.Start();
            progressBar1.Increment(+5);
            if (progressBar1.Value < 100)
            {
            }
            else
            {
                timer1.Stop();
                this.Hide();
                FrmLogin login = new FrmLogin();
                login.Show();
            }
        }
    }
}
