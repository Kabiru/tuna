﻿namespace PatientInfo
{
    partial class frmPatient
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tbPatient = new System.Windows.Forms.TabControl();
            this.tabPagePatientInfo = new System.Windows.Forms.TabPage();
            this.txtMobile = new System.Windows.Forms.TextBox();
            this.lblMobile = new System.Windows.Forms.Label();
            this.txtCity = new System.Windows.Forms.TextBox();
            this.lblCity = new System.Windows.Forms.Label();
            this.txtAddress = new System.Windows.Forms.TextBox();
            this.lblAddress = new System.Windows.Forms.Label();
            this.dateTimePicker1 = new System.Windows.Forms.DateTimePicker();
            this.lblDOB = new System.Windows.Forms.Label();
            this.txtFatherName = new System.Windows.Forms.TextBox();
            this.lblFatherName = new System.Windows.Forms.Label();
            this.cboGender = new System.Windows.Forms.ComboBox();
            this.lblGender = new System.Windows.Forms.Label();
            this.txtLName = new System.Windows.Forms.TextBox();
            this.lblLName = new System.Windows.Forms.Label();
            this.txtFname = new System.Windows.Forms.TextBox();
            this.lblFName = new System.Windows.Forms.Label();
            this.tabPageOperation = new System.Windows.Forms.TabPage();
            this.cboAssistant = new System.Windows.Forms.ComboBox();
            this.cboAnaesthetist = new System.Windows.Forms.ComboBox();
            this.lblAssistant = new System.Windows.Forms.Label();
            this.lblAnaesthetist = new System.Windows.Forms.Label();
            this.cboAnaesthesia = new System.Windows.Forms.ComboBox();
            this.cboSurgeon = new System.Windows.Forms.ComboBox();
            this.txtOperPatientID = new System.Windows.Forms.TextBox();
            this.lblAnaesthesia = new System.Windows.Forms.Label();
            this.lblSurgeon = new System.Windows.Forms.Label();
            this.lblOperPatientID = new System.Windows.Forms.Label();
            this.tabPageDischarge = new System.Windows.Forms.TabPage();
            this.btnSave = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.btnDeath = new System.Windows.Forms.Button();
            this.btnLama = new System.Windows.Forms.Button();
            this.btnSendACall = new System.Windows.Forms.Button();
            this.bntReferal = new System.Windows.Forms.Button();
            this.bntDischarge = new System.Windows.Forms.Button();
            this.dateTimePicker2 = new System.Windows.Forms.DateTimePicker();
            this.lblDichargeDate = new System.Windows.Forms.Label();
            this.txtOperationDate = new System.Windows.Forms.TextBox();
            this.lblOperationDate = new System.Windows.Forms.Label();
            this.txtPhysicalFindings = new System.Windows.Forms.TextBox();
            this.txtClinicalSummary = new System.Windows.Forms.TextBox();
            this.txtFinalDiagnosis = new System.Windows.Forms.TextBox();
            this.txtPatient = new System.Windows.Forms.TextBox();
            this.lblPatient = new System.Windows.Forms.Label();
            this.txtAdmissionDate = new System.Windows.Forms.TextBox();
            this.txtOutgoingType = new System.Windows.Forms.TextBox();
            this.lblPhysicalFindings = new System.Windows.Forms.Label();
            this.lblClinicalSummary = new System.Windows.Forms.Label();
            this.lblFinalDiagnosis = new System.Windows.Forms.Label();
            this.lblAdmissionDate = new System.Windows.Forms.Label();
            this.lblOutgoingType = new System.Windows.Forms.Label();
            this.btnExit = new System.Windows.Forms.Button();
            this.btnNext = new System.Windows.Forms.Button();
            this.btnBack = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.lblPatientID = new System.Windows.Forms.Label();
            this.txtPatientID = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.dateTimePickerOperDate = new System.Windows.Forms.DateTimePicker();
            this.label3 = new System.Windows.Forms.Label();
            this.txtOperID = new System.Windows.Forms.TextBox();
            this.tbPatient.SuspendLayout();
            this.tabPagePatientInfo.SuspendLayout();
            this.tabPageOperation.SuspendLayout();
            this.tabPageDischarge.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // tbPatient
            // 
            this.tbPatient.Controls.Add(this.tabPagePatientInfo);
            this.tbPatient.Controls.Add(this.tabPageOperation);
            this.tbPatient.Controls.Add(this.tabPageDischarge);
            this.tbPatient.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbPatient.Location = new System.Drawing.Point(12, 86);
            this.tbPatient.Name = "tbPatient";
            this.tbPatient.SelectedIndex = 0;
            this.tbPatient.Size = new System.Drawing.Size(716, 231);
            this.tbPatient.TabIndex = 0;
            // 
            // tabPagePatientInfo
            // 
            this.tabPagePatientInfo.Controls.Add(this.txtPatientID);
            this.tabPagePatientInfo.Controls.Add(this.lblPatientID);
            this.tabPagePatientInfo.Controls.Add(this.txtMobile);
            this.tabPagePatientInfo.Controls.Add(this.lblMobile);
            this.tabPagePatientInfo.Controls.Add(this.txtCity);
            this.tabPagePatientInfo.Controls.Add(this.lblCity);
            this.tabPagePatientInfo.Controls.Add(this.txtAddress);
            this.tabPagePatientInfo.Controls.Add(this.lblAddress);
            this.tabPagePatientInfo.Controls.Add(this.dateTimePicker1);
            this.tabPagePatientInfo.Controls.Add(this.lblDOB);
            this.tabPagePatientInfo.Controls.Add(this.txtFatherName);
            this.tabPagePatientInfo.Controls.Add(this.lblFatherName);
            this.tabPagePatientInfo.Controls.Add(this.cboGender);
            this.tabPagePatientInfo.Controls.Add(this.lblGender);
            this.tabPagePatientInfo.Controls.Add(this.txtLName);
            this.tabPagePatientInfo.Controls.Add(this.lblLName);
            this.tabPagePatientInfo.Controls.Add(this.txtFname);
            this.tabPagePatientInfo.Controls.Add(this.lblFName);
            this.tabPagePatientInfo.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tabPagePatientInfo.Location = new System.Drawing.Point(4, 22);
            this.tabPagePatientInfo.Name = "tabPagePatientInfo";
            this.tabPagePatientInfo.Padding = new System.Windows.Forms.Padding(3);
            this.tabPagePatientInfo.Size = new System.Drawing.Size(708, 205);
            this.tabPagePatientInfo.TabIndex = 0;
            this.tabPagePatientInfo.Text = "Patient Info";
            this.tabPagePatientInfo.UseVisualStyleBackColor = true;
            // 
            // txtMobile
            // 
            this.txtMobile.Location = new System.Drawing.Point(311, 167);
            this.txtMobile.Name = "txtMobile";
            this.txtMobile.Size = new System.Drawing.Size(100, 20);
            this.txtMobile.TabIndex = 15;
            // 
            // lblMobile
            // 
            this.lblMobile.AutoSize = true;
            this.lblMobile.Location = new System.Drawing.Point(239, 166);
            this.lblMobile.Name = "lblMobile";
            this.lblMobile.Size = new System.Drawing.Size(38, 13);
            this.lblMobile.TabIndex = 14;
            this.lblMobile.Text = "Mobile";
            // 
            // txtCity
            // 
            this.txtCity.Location = new System.Drawing.Point(69, 166);
            this.txtCity.Name = "txtCity";
            this.txtCity.Size = new System.Drawing.Size(100, 20);
            this.txtCity.TabIndex = 13;
            // 
            // lblCity
            // 
            this.lblCity.AutoSize = true;
            this.lblCity.Location = new System.Drawing.Point(9, 166);
            this.lblCity.Name = "lblCity";
            this.lblCity.Size = new System.Drawing.Size(24, 13);
            this.lblCity.TabIndex = 12;
            this.lblCity.Text = "City";
            // 
            // txtAddress
            // 
            this.txtAddress.Location = new System.Drawing.Point(69, 124);
            this.txtAddress.Name = "txtAddress";
            this.txtAddress.Size = new System.Drawing.Size(342, 20);
            this.txtAddress.TabIndex = 11;
            // 
            // lblAddress
            // 
            this.lblAddress.AutoSize = true;
            this.lblAddress.Location = new System.Drawing.Point(6, 127);
            this.lblAddress.Name = "lblAddress";
            this.lblAddress.Size = new System.Drawing.Size(45, 13);
            this.lblAddress.TabIndex = 10;
            this.lblAddress.Text = "Address";
            // 
            // dateTimePicker1
            // 
            this.dateTimePicker1.Location = new System.Drawing.Point(311, 81);
            this.dateTimePicker1.Name = "dateTimePicker1";
            this.dateTimePicker1.Size = new System.Drawing.Size(200, 20);
            this.dateTimePicker1.TabIndex = 9;
            // 
            // lblDOB
            // 
            this.lblDOB.AutoSize = true;
            this.lblDOB.Location = new System.Drawing.Point(239, 80);
            this.lblDOB.Name = "lblDOB";
            this.lblDOB.Size = new System.Drawing.Size(66, 13);
            this.lblDOB.TabIndex = 8;
            this.lblDOB.Text = "Date of Birth";
            // 
            // txtFatherName
            // 
            this.txtFatherName.Location = new System.Drawing.Point(69, 80);
            this.txtFatherName.Name = "txtFatherName";
            this.txtFatherName.Size = new System.Drawing.Size(100, 20);
            this.txtFatherName.TabIndex = 7;
            // 
            // lblFatherName
            // 
            this.lblFatherName.AutoSize = true;
            this.lblFatherName.Location = new System.Drawing.Point(6, 80);
            this.lblFatherName.Name = "lblFatherName";
            this.lblFatherName.Size = new System.Drawing.Size(47, 13);
            this.lblFatherName.TabIndex = 6;
            this.lblFatherName.Text = "F. Name";
            // 
            // cboGender
            // 
            this.cboGender.FormattingEnabled = true;
            this.cboGender.Location = new System.Drawing.Point(514, 46);
            this.cboGender.Name = "cboGender";
            this.cboGender.Size = new System.Drawing.Size(84, 21);
            this.cboGender.TabIndex = 5;
            // 
            // lblGender
            // 
            this.lblGender.AutoSize = true;
            this.lblGender.Location = new System.Drawing.Point(461, 47);
            this.lblGender.Name = "lblGender";
            this.lblGender.Size = new System.Drawing.Size(27, 13);
            this.lblGender.TabIndex = 4;
            this.lblGender.Text = "M/F";
            // 
            // txtLName
            // 
            this.txtLName.Location = new System.Drawing.Point(311, 46);
            this.txtLName.Name = "txtLName";
            this.txtLName.Size = new System.Drawing.Size(100, 20);
            this.txtLName.TabIndex = 3;
            // 
            // lblLName
            // 
            this.lblLName.AutoSize = true;
            this.lblLName.Location = new System.Drawing.Point(239, 45);
            this.lblLName.Name = "lblLName";
            this.lblLName.Size = new System.Drawing.Size(58, 13);
            this.lblLName.TabIndex = 2;
            this.lblLName.Text = "Last Name";
            // 
            // txtFname
            // 
            this.txtFname.ForeColor = System.Drawing.Color.PaleGreen;
            this.txtFname.Location = new System.Drawing.Point(69, 45);
            this.txtFname.Name = "txtFname";
            this.txtFname.Size = new System.Drawing.Size(100, 20);
            this.txtFname.TabIndex = 1;
            // 
            // lblFName
            // 
            this.lblFName.AutoSize = true;
            this.lblFName.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblFName.Location = new System.Drawing.Point(6, 45);
            this.lblFName.Name = "lblFName";
            this.lblFName.Size = new System.Drawing.Size(57, 13);
            this.lblFName.TabIndex = 0;
            this.lblFName.Text = "First Name";
            // 
            // tabPageOperation
            // 
            this.tabPageOperation.Controls.Add(this.txtOperID);
            this.tabPageOperation.Controls.Add(this.label3);
            this.tabPageOperation.Controls.Add(this.dateTimePickerOperDate);
            this.tabPageOperation.Controls.Add(this.label2);
            this.tabPageOperation.Controls.Add(this.cboAssistant);
            this.tabPageOperation.Controls.Add(this.cboAnaesthetist);
            this.tabPageOperation.Controls.Add(this.lblAssistant);
            this.tabPageOperation.Controls.Add(this.lblAnaesthetist);
            this.tabPageOperation.Controls.Add(this.cboAnaesthesia);
            this.tabPageOperation.Controls.Add(this.cboSurgeon);
            this.tabPageOperation.Controls.Add(this.txtOperPatientID);
            this.tabPageOperation.Controls.Add(this.lblAnaesthesia);
            this.tabPageOperation.Controls.Add(this.lblSurgeon);
            this.tabPageOperation.Controls.Add(this.lblOperPatientID);
            this.tabPageOperation.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tabPageOperation.Location = new System.Drawing.Point(4, 22);
            this.tabPageOperation.Name = "tabPageOperation";
            this.tabPageOperation.Padding = new System.Windows.Forms.Padding(3);
            this.tabPageOperation.Size = new System.Drawing.Size(708, 205);
            this.tabPageOperation.TabIndex = 1;
            this.tabPageOperation.Text = "Operation";
            this.tabPageOperation.UseVisualStyleBackColor = true;
            // 
            // cboAssistant
            // 
            this.cboAssistant.FormattingEnabled = true;
            this.cboAssistant.Location = new System.Drawing.Point(372, 89);
            this.cboAssistant.Name = "cboAssistant";
            this.cboAssistant.Size = new System.Drawing.Size(121, 21);
            this.cboAssistant.TabIndex = 9;
            // 
            // cboAnaesthetist
            // 
            this.cboAnaesthetist.FormattingEnabled = true;
            this.cboAnaesthetist.Location = new System.Drawing.Point(372, 54);
            this.cboAnaesthetist.Name = "cboAnaesthetist";
            this.cboAnaesthetist.Size = new System.Drawing.Size(121, 21);
            this.cboAnaesthetist.TabIndex = 8;
            // 
            // lblAssistant
            // 
            this.lblAssistant.AutoSize = true;
            this.lblAssistant.Location = new System.Drawing.Point(303, 89);
            this.lblAssistant.Name = "lblAssistant";
            this.lblAssistant.Size = new System.Drawing.Size(59, 13);
            this.lblAssistant.TabIndex = 7;
            this.lblAssistant.Text = "Assistant/s";
            // 
            // lblAnaesthetist
            // 
            this.lblAnaesthetist.AutoSize = true;
            this.lblAnaesthetist.Location = new System.Drawing.Point(300, 54);
            this.lblAnaesthetist.Name = "lblAnaesthetist";
            this.lblAnaesthetist.Size = new System.Drawing.Size(59, 13);
            this.lblAnaesthetist.TabIndex = 6;
            this.lblAnaesthetist.Text = "Anesthetist";
            // 
            // cboAnaesthesia
            // 
            this.cboAnaesthesia.FormattingEnabled = true;
            this.cboAnaesthesia.Location = new System.Drawing.Point(89, 87);
            this.cboAnaesthesia.Name = "cboAnaesthesia";
            this.cboAnaesthesia.Size = new System.Drawing.Size(121, 21);
            this.cboAnaesthesia.TabIndex = 5;
            // 
            // cboSurgeon
            // 
            this.cboSurgeon.FormattingEnabled = true;
            this.cboSurgeon.Location = new System.Drawing.Point(89, 52);
            this.cboSurgeon.Name = "cboSurgeon";
            this.cboSurgeon.Size = new System.Drawing.Size(121, 21);
            this.cboSurgeon.TabIndex = 4;
            // 
            // txtOperPatientID
            // 
            this.txtOperPatientID.Location = new System.Drawing.Point(89, 22);
            this.txtOperPatientID.Name = "txtOperPatientID";
            this.txtOperPatientID.Size = new System.Drawing.Size(121, 20);
            this.txtOperPatientID.TabIndex = 3;
            // 
            // lblAnaesthesia
            // 
            this.lblAnaesthesia.AutoSize = true;
            this.lblAnaesthesia.Location = new System.Drawing.Point(7, 87);
            this.lblAnaesthesia.Name = "lblAnaesthesia";
            this.lblAnaesthesia.Size = new System.Drawing.Size(65, 13);
            this.lblAnaesthesia.TabIndex = 2;
            this.lblAnaesthesia.Text = "Anaesthesia";
            // 
            // lblSurgeon
            // 
            this.lblSurgeon.AutoSize = true;
            this.lblSurgeon.Location = new System.Drawing.Point(6, 52);
            this.lblSurgeon.Name = "lblSurgeon";
            this.lblSurgeon.Size = new System.Drawing.Size(47, 13);
            this.lblSurgeon.TabIndex = 1;
            this.lblSurgeon.Text = "Surgeon";
            // 
            // lblOperPatientID
            // 
            this.lblOperPatientID.AutoSize = true;
            this.lblOperPatientID.Location = new System.Drawing.Point(7, 22);
            this.lblOperPatientID.Name = "lblOperPatientID";
            this.lblOperPatientID.Size = new System.Drawing.Size(54, 13);
            this.lblOperPatientID.TabIndex = 0;
            this.lblOperPatientID.Text = "Patient ID";
            // 
            // tabPageDischarge
            // 
            this.tabPageDischarge.Controls.Add(this.btnSave);
            this.tabPageDischarge.Controls.Add(this.groupBox1);
            this.tabPageDischarge.Controls.Add(this.dateTimePicker2);
            this.tabPageDischarge.Controls.Add(this.lblDichargeDate);
            this.tabPageDischarge.Controls.Add(this.txtOperationDate);
            this.tabPageDischarge.Controls.Add(this.lblOperationDate);
            this.tabPageDischarge.Controls.Add(this.txtPhysicalFindings);
            this.tabPageDischarge.Controls.Add(this.txtClinicalSummary);
            this.tabPageDischarge.Controls.Add(this.txtFinalDiagnosis);
            this.tabPageDischarge.Controls.Add(this.txtPatient);
            this.tabPageDischarge.Controls.Add(this.lblPatient);
            this.tabPageDischarge.Controls.Add(this.txtAdmissionDate);
            this.tabPageDischarge.Controls.Add(this.txtOutgoingType);
            this.tabPageDischarge.Controls.Add(this.lblPhysicalFindings);
            this.tabPageDischarge.Controls.Add(this.lblClinicalSummary);
            this.tabPageDischarge.Controls.Add(this.lblFinalDiagnosis);
            this.tabPageDischarge.Controls.Add(this.lblAdmissionDate);
            this.tabPageDischarge.Controls.Add(this.lblOutgoingType);
            this.tabPageDischarge.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tabPageDischarge.Location = new System.Drawing.Point(4, 22);
            this.tabPageDischarge.Name = "tabPageDischarge";
            this.tabPageDischarge.Size = new System.Drawing.Size(708, 205);
            this.tabPageDischarge.TabIndex = 2;
            this.tabPageDischarge.Text = "Discharge";
            this.tabPageDischarge.UseVisualStyleBackColor = true;
            // 
            // btnSave
            // 
            this.btnSave.Location = new System.Drawing.Point(610, 173);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(75, 23);
            this.btnSave.TabIndex = 17;
            this.btnSave.Text = "Save";
            this.btnSave.UseVisualStyleBackColor = true;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.btnDeath);
            this.groupBox1.Controls.Add(this.btnLama);
            this.groupBox1.Controls.Add(this.btnSendACall);
            this.groupBox1.Controls.Add(this.bntReferal);
            this.groupBox1.Controls.Add(this.bntDischarge);
            this.groupBox1.Location = new System.Drawing.Point(591, 3);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(114, 164);
            this.groupBox1.TabIndex = 16;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Outgoing Options";
            // 
            // btnDeath
            // 
            this.btnDeath.Location = new System.Drawing.Point(19, 131);
            this.btnDeath.Name = "btnDeath";
            this.btnDeath.Size = new System.Drawing.Size(75, 23);
            this.btnDeath.TabIndex = 4;
            this.btnDeath.Text = "Death";
            this.btnDeath.UseVisualStyleBackColor = true;
            this.btnDeath.Click += new System.EventHandler(this.button7_Click);
            // 
            // btnLama
            // 
            this.btnLama.Location = new System.Drawing.Point(19, 104);
            this.btnLama.Name = "btnLama";
            this.btnLama.Size = new System.Drawing.Size(75, 23);
            this.btnLama.TabIndex = 3;
            this.btnLama.Text = "LAMA";
            this.btnLama.UseVisualStyleBackColor = true;
            // 
            // btnSendACall
            // 
            this.btnSendACall.Location = new System.Drawing.Point(19, 75);
            this.btnSendACall.Name = "btnSendACall";
            this.btnSendACall.Size = new System.Drawing.Size(75, 23);
            this.btnSendACall.TabIndex = 2;
            this.btnSendACall.Text = "Send a Call";
            this.btnSendACall.UseVisualStyleBackColor = true;
            // 
            // bntReferal
            // 
            this.bntReferal.Location = new System.Drawing.Point(19, 46);
            this.bntReferal.Name = "bntReferal";
            this.bntReferal.Size = new System.Drawing.Size(75, 23);
            this.bntReferal.TabIndex = 1;
            this.bntReferal.Text = "Referal";
            this.bntReferal.UseVisualStyleBackColor = true;
            // 
            // bntDischarge
            // 
            this.bntDischarge.Location = new System.Drawing.Point(19, 17);
            this.bntDischarge.Name = "bntDischarge";
            this.bntDischarge.Size = new System.Drawing.Size(75, 23);
            this.bntDischarge.TabIndex = 0;
            this.bntDischarge.Text = "Discharge";
            this.bntDischarge.UseVisualStyleBackColor = true;
            // 
            // dateTimePicker2
            // 
            this.dateTimePicker2.Location = new System.Drawing.Point(400, 13);
            this.dateTimePicker2.Name = "dateTimePicker2";
            this.dateTimePicker2.Size = new System.Drawing.Size(170, 20);
            this.dateTimePicker2.TabIndex = 15;
            // 
            // lblDichargeDate
            // 
            this.lblDichargeDate.AutoSize = true;
            this.lblDichargeDate.Location = new System.Drawing.Point(447, 36);
            this.lblDichargeDate.Name = "lblDichargeDate";
            this.lblDichargeDate.Size = new System.Drawing.Size(81, 13);
            this.lblDichargeDate.TabIndex = 14;
            this.lblDichargeDate.Text = "Discharge Date";
            // 
            // txtOperationDate
            // 
            this.txtOperationDate.Location = new System.Drawing.Point(284, 40);
            this.txtOperationDate.Name = "txtOperationDate";
            this.txtOperationDate.Size = new System.Drawing.Size(100, 20);
            this.txtOperationDate.TabIndex = 13;
            // 
            // lblOperationDate
            // 
            this.lblOperationDate.AutoSize = true;
            this.lblOperationDate.Location = new System.Drawing.Point(199, 40);
            this.lblOperationDate.Name = "lblOperationDate";
            this.lblOperationDate.Size = new System.Drawing.Size(79, 13);
            this.lblOperationDate.TabIndex = 12;
            this.lblOperationDate.Text = "Operation Date";
            // 
            // txtPhysicalFindings
            // 
            this.txtPhysicalFindings.Location = new System.Drawing.Point(95, 130);
            this.txtPhysicalFindings.Name = "txtPhysicalFindings";
            this.txtPhysicalFindings.Size = new System.Drawing.Size(475, 20);
            this.txtPhysicalFindings.TabIndex = 11;
            // 
            // txtClinicalSummary
            // 
            this.txtClinicalSummary.Location = new System.Drawing.Point(95, 100);
            this.txtClinicalSummary.Name = "txtClinicalSummary";
            this.txtClinicalSummary.Size = new System.Drawing.Size(475, 20);
            this.txtClinicalSummary.TabIndex = 10;
            // 
            // txtFinalDiagnosis
            // 
            this.txtFinalDiagnosis.Location = new System.Drawing.Point(95, 71);
            this.txtFinalDiagnosis.Name = "txtFinalDiagnosis";
            this.txtFinalDiagnosis.Size = new System.Drawing.Size(475, 20);
            this.txtFinalDiagnosis.TabIndex = 9;
            // 
            // txtPatient
            // 
            this.txtPatient.Location = new System.Drawing.Point(284, 14);
            this.txtPatient.Name = "txtPatient";
            this.txtPatient.Size = new System.Drawing.Size(100, 20);
            this.txtPatient.TabIndex = 8;
            // 
            // lblPatient
            // 
            this.lblPatient.AutoSize = true;
            this.lblPatient.Location = new System.Drawing.Point(199, 13);
            this.lblPatient.Name = "lblPatient";
            this.lblPatient.Size = new System.Drawing.Size(40, 13);
            this.lblPatient.TabIndex = 7;
            this.lblPatient.Text = "Patient";
            // 
            // txtAdmissionDate
            // 
            this.txtAdmissionDate.Location = new System.Drawing.Point(93, 44);
            this.txtAdmissionDate.Name = "txtAdmissionDate";
            this.txtAdmissionDate.Size = new System.Drawing.Size(100, 20);
            this.txtAdmissionDate.TabIndex = 6;
            // 
            // txtOutgoingType
            // 
            this.txtOutgoingType.Location = new System.Drawing.Point(93, 13);
            this.txtOutgoingType.Name = "txtOutgoingType";
            this.txtOutgoingType.Size = new System.Drawing.Size(100, 20);
            this.txtOutgoingType.TabIndex = 5;
            // 
            // lblPhysicalFindings
            // 
            this.lblPhysicalFindings.AutoSize = true;
            this.lblPhysicalFindings.Location = new System.Drawing.Point(3, 130);
            this.lblPhysicalFindings.Name = "lblPhysicalFindings";
            this.lblPhysicalFindings.Size = new System.Drawing.Size(88, 13);
            this.lblPhysicalFindings.TabIndex = 4;
            this.lblPhysicalFindings.Text = "Physical Findings";
            // 
            // lblClinicalSummary
            // 
            this.lblClinicalSummary.AutoSize = true;
            this.lblClinicalSummary.Location = new System.Drawing.Point(3, 103);
            this.lblClinicalSummary.Name = "lblClinicalSummary";
            this.lblClinicalSummary.Size = new System.Drawing.Size(86, 13);
            this.lblClinicalSummary.TabIndex = 3;
            this.lblClinicalSummary.Text = "Clinical Summary";
            // 
            // lblFinalDiagnosis
            // 
            this.lblFinalDiagnosis.AutoSize = true;
            this.lblFinalDiagnosis.Location = new System.Drawing.Point(3, 71);
            this.lblFinalDiagnosis.Name = "lblFinalDiagnosis";
            this.lblFinalDiagnosis.Size = new System.Drawing.Size(78, 13);
            this.lblFinalDiagnosis.TabIndex = 2;
            this.lblFinalDiagnosis.Text = "Final Diagnosis";
            this.lblFinalDiagnosis.Click += new System.EventHandler(this.label4_Click);
            // 
            // lblAdmissionDate
            // 
            this.lblAdmissionDate.AutoSize = true;
            this.lblAdmissionDate.Location = new System.Drawing.Point(3, 44);
            this.lblAdmissionDate.Name = "lblAdmissionDate";
            this.lblAdmissionDate.Size = new System.Drawing.Size(80, 13);
            this.lblAdmissionDate.TabIndex = 1;
            this.lblAdmissionDate.Text = "Admission Date";
            // 
            // lblOutgoingType
            // 
            this.lblOutgoingType.AutoSize = true;
            this.lblOutgoingType.Location = new System.Drawing.Point(3, 13);
            this.lblOutgoingType.Name = "lblOutgoingType";
            this.lblOutgoingType.Size = new System.Drawing.Size(77, 13);
            this.lblOutgoingType.TabIndex = 0;
            this.lblOutgoingType.Text = "Outgoing Type";
            // 
            // btnExit
            // 
            this.btnExit.Location = new System.Drawing.Point(626, 345);
            this.btnExit.Name = "btnExit";
            this.btnExit.Size = new System.Drawing.Size(75, 23);
            this.btnExit.TabIndex = 1;
            this.btnExit.Text = "E&xit";
            this.btnExit.UseVisualStyleBackColor = true;
            this.btnExit.Click += new System.EventHandler(this.btnExit_Click);
            // 
            // btnNext
            // 
            this.btnNext.Location = new System.Drawing.Point(530, 345);
            this.btnNext.Name = "btnNext";
            this.btnNext.Size = new System.Drawing.Size(75, 23);
            this.btnNext.TabIndex = 2;
            this.btnNext.Text = "N&ext>>";
            this.btnNext.UseVisualStyleBackColor = true;
            this.btnNext.Click += new System.EventHandler(this.btnNext_Click);
            // 
            // btnBack
            // 
            this.btnBack.Location = new System.Drawing.Point(426, 345);
            this.btnBack.Name = "btnBack";
            this.btnBack.Size = new System.Drawing.Size(75, 23);
            this.btnBack.TabIndex = 3;
            this.btnBack.Text = "<<&Back";
            this.btnBack.UseVisualStyleBackColor = true;
            this.btnBack.Click += new System.EventHandler(this.btnBack_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Poor Richard", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(198, 29);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(303, 36);
            this.label1.TabIndex = 4;
            this.label1.Text = "Patient Profile Details";
            // 
            // lblPatientID
            // 
            this.lblPatientID.AutoSize = true;
            this.lblPatientID.Location = new System.Drawing.Point(9, 7);
            this.lblPatientID.Name = "lblPatientID";
            this.lblPatientID.Size = new System.Drawing.Size(54, 13);
            this.lblPatientID.TabIndex = 16;
            this.lblPatientID.Text = "Patient ID";
            // 
            // txtPatientID
            // 
            this.txtPatientID.Location = new System.Drawing.Point(69, 7);
            this.txtPatientID.Name = "txtPatientID";
            this.txtPatientID.Size = new System.Drawing.Size(100, 20);
            this.txtPatientID.TabIndex = 17;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(306, 21);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(79, 13);
            this.label2.TabIndex = 10;
            this.label2.Text = "Operation Date";
            // 
            // dateTimePickerOperDate
            // 
            this.dateTimePickerOperDate.Location = new System.Drawing.Point(391, 19);
            this.dateTimePickerOperDate.Name = "dateTimePickerOperDate";
            this.dateTimePickerOperDate.Size = new System.Drawing.Size(200, 20);
            this.dateTimePickerOperDate.TabIndex = 11;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(10, 126);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(67, 13);
            this.label3.TabIndex = 12;
            this.label3.Text = "Operation ID";
            // 
            // txtOperID
            // 
            this.txtOperID.Location = new System.Drawing.Point(89, 126);
            this.txtOperID.Name = "txtOperID";
            this.txtOperID.Size = new System.Drawing.Size(100, 20);
            this.txtOperID.TabIndex = 13;
            // 
            // frmPatient
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(744, 405);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.btnBack);
            this.Controls.Add(this.btnNext);
            this.Controls.Add(this.btnExit);
            this.Controls.Add(this.tbPatient);
            this.Name = "frmPatient";
            this.Text = "frmPatient";
            this.Load += new System.EventHandler(this.frmPatient_Load);
            this.tbPatient.ResumeLayout(false);
            this.tabPagePatientInfo.ResumeLayout(false);
            this.tabPagePatientInfo.PerformLayout();
            this.tabPageOperation.ResumeLayout(false);
            this.tabPageOperation.PerformLayout();
            this.tabPageDischarge.ResumeLayout(false);
            this.tabPageDischarge.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TabControl tbPatient;
        private System.Windows.Forms.TabPage tabPagePatientInfo;
        private System.Windows.Forms.TextBox txtMobile;
        private System.Windows.Forms.Label lblMobile;
        private System.Windows.Forms.TextBox txtCity;
        private System.Windows.Forms.Label lblCity;
        private System.Windows.Forms.TextBox txtAddress;
        private System.Windows.Forms.Label lblAddress;
        private System.Windows.Forms.DateTimePicker dateTimePicker1;
        private System.Windows.Forms.Label lblDOB;
        private System.Windows.Forms.TextBox txtFatherName;
        private System.Windows.Forms.Label lblFatherName;
        private System.Windows.Forms.ComboBox cboGender;
        private System.Windows.Forms.Label lblGender;
        private System.Windows.Forms.TextBox txtLName;
        private System.Windows.Forms.Label lblLName;
        private System.Windows.Forms.TextBox txtFname;
        private System.Windows.Forms.Label lblFName;
        private System.Windows.Forms.TabPage tabPageOperation;
        private System.Windows.Forms.ComboBox cboAssistant;
        private System.Windows.Forms.ComboBox cboAnaesthetist;
        private System.Windows.Forms.Label lblAssistant;
        private System.Windows.Forms.Label lblAnaesthetist;
        private System.Windows.Forms.ComboBox cboAnaesthesia;
        private System.Windows.Forms.ComboBox cboSurgeon;
        private System.Windows.Forms.TextBox txtOperPatientID;
        private System.Windows.Forms.Label lblAnaesthesia;
        private System.Windows.Forms.Label lblSurgeon;
        private System.Windows.Forms.Label lblOperPatientID;
        private System.Windows.Forms.TabPage tabPageDischarge;
        private System.Windows.Forms.TextBox txtFinalDiagnosis;
        private System.Windows.Forms.TextBox txtPatient;
        private System.Windows.Forms.Label lblPatient;
        private System.Windows.Forms.TextBox txtAdmissionDate;
        private System.Windows.Forms.TextBox txtOutgoingType;
        private System.Windows.Forms.Label lblPhysicalFindings;
        private System.Windows.Forms.Label lblClinicalSummary;
        private System.Windows.Forms.Label lblFinalDiagnosis;
        private System.Windows.Forms.Label lblAdmissionDate;
        private System.Windows.Forms.Label lblOutgoingType;
        private System.Windows.Forms.Button btnExit;
        private System.Windows.Forms.Button btnNext;
        private System.Windows.Forms.Button btnBack;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button btnDeath;
        private System.Windows.Forms.Button btnLama;
        private System.Windows.Forms.Button btnSendACall;
        private System.Windows.Forms.Button bntReferal;
        private System.Windows.Forms.Button bntDischarge;
        private System.Windows.Forms.DateTimePicker dateTimePicker2;
        private System.Windows.Forms.Label lblDichargeDate;
        private System.Windows.Forms.TextBox txtOperationDate;
        private System.Windows.Forms.Label lblOperationDate;
        private System.Windows.Forms.TextBox txtPhysicalFindings;
        private System.Windows.Forms.TextBox txtClinicalSummary;
        private System.Windows.Forms.Button btnSave;
        private System.Windows.Forms.TextBox txtPatientID;
        private System.Windows.Forms.Label lblPatientID;
        private System.Windows.Forms.DateTimePicker dateTimePickerOperDate;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtOperID;
        private System.Windows.Forms.Label label3;
    }
}