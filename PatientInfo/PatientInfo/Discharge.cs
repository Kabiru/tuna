﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PatientInfo
{
    class Discharge
    {
        protected string dischargepatientid;
        protected string outgoingpatient;
        protected string date;
        protected string diagnosis;
        protected string clinicalsummary;
        protected string physicalfindings;
        protected string laboratoryfindings;
        protected string time;

        public  Discharge(string _dischargepatientid, string _outgoingpatient, string _date,
                              string _diagnosis, string _clinicalsummary, string _physicalfindings, string _laboratoryfindings, string _time) 
        {
            //initializing data members
            dischargepatientid = _dischargepatientid;
            outgoingpatient = _outgoingpatient;
            date = _date;
            diagnosis = _diagnosis;
            clinicalsummary = _clinicalsummary;
            physicalfindings = _physicalfindings;
            laboratoryfindings = _laboratoryfindings;
            time = _time;
        }

        public void insert()
        {
            Main code = new Main();
            code.Execute(@"
                INSERT INTO [dbo].[Discharge]
                ([DIS_Discharge_Patient_Id], [DIS_Outgoing_Type], [DIS_DATE])
                VALUES
                ('"+dischargepatientid+"', '"+outgoingpatient+"', Convert(smalldatetime,'"+date+"',103))");
        }
    }
}
