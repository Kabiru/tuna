﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PatientInfo
{
    class Surgery
    {
        protected string operdate;
        protected string operid;
        protected string operpatientid;
        protected string opersurgeon;
        protected string operanaesthetist;
        protected string operassistant;

        public Surgery(string _operdate, string _operid, string _operpatientid,
                            string _opersurgeon, string _operanaesthetist, string _operassistant)
        {
           //initializing data members
            operdate = _operdate;
            operid = _operid;
            operpatientid = _operpatientid;
            opersurgeon = _opersurgeon;
            operanaesthetist = _operanaesthetist;
            operassistant = _operassistant;
        }

        public void insert()
        {
            Main code = new Main();
            code.Execute(@"
                 INSERT INTO [dbo].[Patient_surgery_Detail]
                 ([OPR_Oper_Date], [OPR_Oper_Id], [OPR_Oper_Patient_Id],
                 [OPR_Oper_Surgeon], [OPR_Oper_Anaesthetist], [OPR_Oper_Assistant])
                VALUES
                 (Convert(smalldatetime,'"+operdate+"', 103), '"+operid+"', '"+operpatientid+"', '"+opersurgeon+"', '"+operanaesthetist+"', '"+operassistant+"')");

        }
    }
}
