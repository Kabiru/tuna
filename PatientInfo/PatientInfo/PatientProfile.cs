﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PatientInfo
{
    class PatientProfile
    {
        protected string Patientid;
        protected string Patientfirstname;
        protected string patientlastname;
        protected string Gender;
        protected string Father_Name;
        protected string dateofbirth;
        protected string address;
        protected string City;
        protected string Phone;
       /* private string p1;
        private string p2;
        private string p3;
        private string p4;
        private string p5;
        private string p6;
        private string p7;
        private string p8;
        private string p9;*/

       /* public PatientProfile(string p1, string p2, string p3, string p4, string p5, string p6, string p7, string p8, string p9)
        {
            // TODO: Complete member initialization
            this.p1 = p1;
            this.p2 = p2;
            this.p3 = p3;
            this.p4 = p4;
            this.p5 = p5;
            this.p6 = p6;
            this.p7 = p7;
            this.p8 = p8;
            this.p9 = p9;
        }*/

        public  PatientProfile(string _Patientid, string _Patientfirstname, string _patientlastname,
                            string _Gender, string _Father_Name, string _dateofbirth, string _address
                            , string _City, string _Phone )
        {
           //constructor; initialising protected to public data members
            Patientid = _Patientid;
            Patientfirstname = _Patientfirstname;
            patientlastname = _patientlastname;
            Gender = _Gender;
            Father_Name = _Father_Name;
            dateofbirth = _dateofbirth;
            address = _address;
            City = _City;
            Phone = _Phone;
        }

        public void insert() 
        {
            Main code = new Main();
            code.Execute(@"
                INSERT INTO [dbo].[Patient_Info_Detail]
                    ([CRE_Patient_id],[CRE_FirstName], 
                     [CRE_LastName], [CRE_Gender]
                     ,[CRE_Father_Name], [CRE_Date_Of_Birth]),
                     [CRE_Address], [CRE_Phone1])
                VALUES
                    ('"+Patientid+"', '"+Patientfirstname+"', '"+patientlastname+"','"+Gender+"','"+Father_Name+"', Convert(smalldatetime,'"+dateofbirth+"', 103), '"+address+"', '"+Phone+"')");

        }
    }
}
