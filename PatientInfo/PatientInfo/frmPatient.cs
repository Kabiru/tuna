﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PatientInfo
{
    public partial class frmPatient : Form
    {
        public frmPatient()
        {
            InitializeComponent();
        }

        private void label4_Click(object sender, EventArgs e)
        {

        }

        private void button7_Click(object sender, EventArgs e)
        {

        }

        private void btnExit_Click(object sender, EventArgs e)
        {
            this.Close();//close application
        }

        private void frmPatient_Load(object sender, EventArgs e)
        {

        }

        private void btnBack_Click(object sender, EventArgs e)
        {
            //back button
            if (tbPatient.SelectedTab.Text == "Discharge")
            {
                //back to operation
                tbPatient.SelectedTab = tabPageOperation;
            }
            else if(tbPatient.SelectedTab.Text == "Operation") { 
                //back to patient info
                tbPatient.SelectedTab = tabPagePatientInfo;
            }
        }

        private void btnNext_Click(object sender, EventArgs e)
        {
            //next button
            if (tbPatient.SelectedTab.Text == "Patient Info") 
            {
                //next to operation
                tbPatient.SelectedTab = tabPageOperation;
            }
            else if (tbPatient.SelectedTab.Text == "Operation")
            {
                //next discharge
                tbPatient.SelectedTab = tabPageDischarge;
            }
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            DateTime today = DateTime.Today;
            
            //patient profile details
            PatientProfile patinfo = new PatientProfile(txtPatientID.Text,txtFname.Text, txtLName.Text, cboGender.Text,txtFatherName.Text, 
                dateTimePicker1.Text, txtAddress.Text, txtCity.Text, txtMobile.Text );
            patinfo.insert();

            //operation details
            Surgery sur = new  Surgery(dateTimePickerOperDate.Text, txtOperID.Text, txtPatientID.Text, cboSurgeon.Text, 
                cboAnaesthetist.Text, cboAssistant.Text);
            sur.insert();//inserting into the database
            
            //discharge details
            Discharge dis = new Discharge();
        }
    }
}
