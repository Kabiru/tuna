﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Lab_portfolio
{
    public partial class frmIfProgram : Form
    {
        public frmIfProgram()
        {
            InitializeComponent();
        }

        private void btnGrade_Click(object sender, EventArgs e)
        {

            if (txtRegNo.Text.Length == 0)
            {
                errorProvider1.SetError(txtRegNo, "Registration Number Is Required");
                return;
            }
            else
            {
                errorProvider1.SetError(txtRegNo, null);
            }

            int catone, cattwo, finalexam, average;
            char Grade = ' ';
            string Comment = " ";

            catone = Convert.ToInt32(txtCatOne.Text);
            cattwo = Convert.ToInt32(txtCatTwo.Text);
            finalexam = Convert.ToInt32(txtExam.Text);
            average = catone + cattwo + finalexam;

            if(average >= 70 && average <= 100)
            {
                Grade = 'A';
                Comment = "Excellent";
            }
            if(average >= 60 && average <= 69)
            {
                Grade = 'B';
                Comment = "Good";
            }
            if(average >= 50 && average <= 59)
            {
                Grade = 'C';
                Comment = "Fair";
            }
            if(average >= 40 && average <= 49)
            {
                Grade = 'D';
                Comment = "Poor";
            }
            if(average >= 0 && average <= 39)
            {
                Grade = 'F';
                Comment = "Fail";
            }

            dgvStudent.Rows.Add(txtRegNo.Text, txtStuName.Text, txtUName.Text, txtUCode.Text, catone, cattwo, finalexam, average, Grade, Comment);
        }

        private void txtCatOne_Validating(object sender, CancelEventArgs e)
        {
            int catonemark;
            if(int.TryParse(txtCatOne.Text, out catonemark))
            {
                //Marks are Numeric
                if(catonemark <0 || catonemark >15)
                {
                    MessageBox.Show("Cat Marks cannot be more than 15 or less than 0");
                    txtCatOne.Clear();//clears all the content in the textbox;
                }
            }
            else
            {
                //Marks are not Numeric
                MessageBox.Show("Invalid Marks");
                txtCatOne.Clear();//clears all the content in the textbox;
            }
        }
    }
}
