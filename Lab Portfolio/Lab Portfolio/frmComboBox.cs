﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Lab_Portfolio
{
    public partial class frmComboBox : Form
    {
        public frmComboBox()
        {
            InitializeComponent();
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            cboNationality.Items.Clear();//clearing items on combo box
            cboNationality.Items.Add("Kenya");
            cboNationality.Items.Add("Tanzania");
            cboNationality.Items.Add("Uganda");
            cboNationality.Items.Add("Somalia");
            //adding items into combo box
        }

        private void btnRemove_Click(object sender, EventArgs e)
        {
            cboNationality.Items.Remove(cboNationality.SelectedItem);
            //removing the selected items
        }

        private void btnCount_Click(object sender, EventArgs e)
        {
            MessageBox.Show("The total number of items is: " + cboNationality.Items.Count);
            //counting the number of items
        }
    }
}
