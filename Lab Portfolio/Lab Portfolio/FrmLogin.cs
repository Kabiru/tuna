﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Lab_Portfolio
{
    public partial class FrmLogin : Form
    {
        public FrmLogin()
        {
            InitializeComponent();
        }


        //log In click event
        private void btnLogin_Click(object sender, EventArgs e)
        {
            string password, username;

            username = txtUserName.Text;
            password = txtPassword.Text;

            if (password == "1234" && username == "root")
            {

                GlobalVariables gv = new GlobalVariables();

                GlobalVariables.user = username;

                MessageBox.Show("Access Granted");
                FrmMain main = new FrmMain();
                this.Hide();
                main.Show();
            }
            else {
                MessageBox.Show("invalid login Credentials");
                txtUserName.Clear();
                txtPassword.Clear();//clearing the text boxes.
            }
        }

        private void txtUserName_TextChanged(object sender, EventArgs e)
        {

        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            Application.Exit();//Exiting application
        }
    }
}
