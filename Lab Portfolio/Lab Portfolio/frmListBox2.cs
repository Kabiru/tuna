﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Lab_Portfolio
{
    public partial class frmListBox2 : Form
    {
        public frmListBox2()
        {
            InitializeComponent();//initialises the ionterface

            lstCourses.Items.Add("Visual Programming");
            lstCourses.Items.Add("Database Systems");
            lstCourses.Items.Add("Logic Circuits");
            lstCourses.Items.Add("Differential Calculus");
            lstCourses.Items.Add("Semiconductor Theory");
            lstCourses.Items.Add("OOP II Theory");
            lstCourses.Items.Add("OOP II Lab");
            //adding data to course listbox

            lblCourses.Text = "Available courses: " + lstCourses.Items.Count;
            lblRegistered.Text = "Registered Courses: " + lstRegistered.Items.Count;

        }

        private void frmListBox2_Load(object sender, EventArgs e)
        {

        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            try
            {
                lstRegistered.Items.Add(lstCourses.SelectedItem);
                //adding the selected item from course to register
                lstCourses.Items.Remove(lstCourses.SelectedItem);
                //removing the transferred item
                
                lblCourses.Text = "Available courses: " + lstCourses.Items.Count;
                lblRegistered.Text = "Registered Courses: " + lstRegistered.Items.Count;           
                //Counting 
            }
            catch {
                MessageBox.Show("Please Select a course to continue");
                return;
            }
        }

        private void btnRemove_Click(object sender, EventArgs e)
        {
            try
            {
                lstCourses.Items.Add(lstRegistered.SelectedItem);
                //adding the selected item from course to register
                lstRegistered.Items.Remove(lstRegistered.SelectedItem);
                //removing the transferred item

                lblCourses.Text = "Available courses: " + lstCourses.Items.Count;
                lblRegistered.Text = "Registered Courses: " + lstRegistered.Items.Count;
            }
            catch 
            {
                MessageBox.Show("Please select a course to continue");
                return;
            }
        }

        private void lstCourses_SelectedIndexChanged(object sender, EventArgs e)
        {

        }
    }
}
