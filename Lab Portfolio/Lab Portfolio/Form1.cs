﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Lab_Portfolio
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            const double VAT = 0.16;//declaring constant
            String Pname;
            int Pcode;
            double pprice, VATCharged, totalAmount;//variable declarations

            Pname = txtPname.Text;
            pprice = Convert.ToDouble(txtPprice.Text);
            Pcode = Convert.ToInt32(txtPcode.Text);

            VATCharged = VAT * pprice;//computing the vat charged
            totalAmount = VATCharged + pprice;//computing total amount
            dgvProducts.Rows.Add(Pcode, Pname, pprice, VATCharged, totalAmount);
        }

        private void button2_Click(object sender, EventArgs e)
        {
            dgvProducts.Rows.Clear();//clearing rows from data grid view
        }
    }
}
