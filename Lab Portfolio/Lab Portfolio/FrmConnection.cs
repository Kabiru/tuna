﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using MySql.Data.MySqlClient;

namespace Lab_Portfolio
{
    public partial class FrmConnection : Form
    {
        public FrmConnection()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                String myConnection = "datasource = localhost; port = 3306; username=root; password=allegromanontroppo";
                MySqlConnection myConn = new MySqlConnection(myConnection);
                MySqlDataAdapter myDataAdapter = new MySqlDataAdapter();
                myDataAdapter.SelectCommand = new MySqlCommand("select * from sakila.actor; ", myConn);
                MySqlCommandBuilder cb = new MySqlCommandBuilder(myDataAdapter);

                myConn.Open();

                MessageBox.Show("Connected");
                myConn.Close();
            }
            catch (Exception ex) {
                MessageBox.Show(ex.Message);
            }
        }

        private void pgbConnecting_Click(object sender, EventArgs e)
        {

        }
    }
}
