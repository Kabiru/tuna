﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Lab_Portfolio
{
    public partial class frmPictureBox : Form
    {
        public frmPictureBox()
        {
            InitializeComponent();
        }

        private void btnBrowse_Click(object sender, EventArgs e)
        {
            OpenFileDialog dg = new OpenFileDialog();//creating a new instance
            //OpenFileDialog - browse for files within local machine
            dg.Title = "Select Image";

            if (dg.ShowDialog() == DialogResult.OK) {
                picSample.ImageLocation = dg.FileName;
            }
        }
    }
}
