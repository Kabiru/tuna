﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Lab_Portfolio
{
    public partial class frmStudentCampus : Form
    {
        public frmStudentCampus()
        {
            InitializeComponent();

            //String FName, LName, DOB, Gender;
            //int RegNO;
        }

        private void textBox4_TextChanged(object sender, EventArgs e)
        {

        }

        private void cboGender_SelectedIndexChanged(object sender, EventArgs e)
        {
            cboGender.Items.Add("Male");
            cboGender.Items.Add("Female");
            //gender choices
        }

        private void tabPage2_Click(object sender, EventArgs e)
        {

        }

        private void btnExitCourse_Click(object sender, EventArgs e)
        {

        }

        private void textBox4_TextChanged_1(object sender, EventArgs e)
        {

        }

        private void cboUnits_SelectedIndexChanged(object sender, EventArgs e)
        {
            cboUnits.Items.Add("CMT 101 Fundumentals of Computing");
            cboUnits.Items.Add("CMT 102 Database Systems");
            cboUnits.Items.Add("CMT 201 Visual Programming I");
            cboUnits.Items.Add("CMT 202 Visual Programming II");
            cboUnits.Items.Add("CMT 301 OOP II");
            cboUnits.Items.Add("CMT 302 OOP II Lab");
            cboUnits.Items.Add("CMT 401 Data Structures and Algorithms");
            cboUnits.Items.Add("CMT 402 Fundumentals of Software Engineering");
        }

        private void textBox3_TextChanged(object sender, EventArgs e)
        {           

        }

        private void btnNextCourse_Click(object sender, EventArgs e)
        {
            /*if (!rdoYear1.Checked || !rdoYear2.Checked || !rdoYear3.Checked || !rdoYear4.Checked) {
                MessageBox.Show("Year Required");
                return;
            }*/
            if (cboUnits.Text == "") {
                MessageBox.Show("Unit Required");
                cboUnits.Focus();
                return;
            }
            if (txtCat1.Text == "") {
                MessageBox.Show("Cat1 Marks Required");
                txtCat1.Focus();
                return;
            }
            if (txtCat2.Text == "") {
                MessageBox.Show("Cat2 Marks Required");
                txtCat2.Focus();
                return;
            }
            if (txtExam.Text == "") {
                MessageBox.Show("Exam Marks Required");
                txtExam.Focus();
                return;
            }

            int Cat1, Cat2, Exam;
            char Grade;
            double Aggregate;
            
            Cat1 = Convert.ToInt32(txtCat1.Text);
            Cat2 = Convert.ToInt32(txtCat2.Text);
            Exam = Convert.ToInt32(txtExam.Text);
            //fetching input

            if (Cat1 > 20) {
                MessageBox.Show("Invalid Cat1 Marks");
                txtCat1.Focus();
                return;
            }
            if (Cat2 > 30) {
                MessageBox.Show("Invalid Cat2 Marks");
                txtCat2.Focus();
                return;
            }
            if (Exam > 50) {
                MessageBox.Show("Invalid Exam Marks");
                txtExam.Focus();
                return;
            }

            Aggregate = (Cat1 + Cat2 + Exam);

            if (Aggregate >= 75) {
                Grade = 'A';
            }
            else if (Aggregate >= 65 && Aggregate < 75){
                Grade = 'B';
            }
            else if (Aggregate >= 55 && Aggregate < 65){
                Grade = 'C';
            }
            else if (Aggregate >=45 && Aggregate < 55){
                Grade = 'D';
            }
            else if (Aggregate <45 && Aggregate != 0){
                Grade = 'F';
            }
            else {
              Grade = 'I';
            }

            //dgvStudentGrade.Rows.Add(Grade);
            dgvStudentGrade.Rows.Add(txtFirstName.Text, txtLastName.Text, cboGender.Text,dateTimePicker1.Text, txtRegNo, Grade);

            tabControl1.SelectTab(2);
        }

        private void btnNextDetails_Click(object sender, EventArgs e)
        {
            if (txtFirstName.Text == "") {
                MessageBox.Show("First Name Required");
                txtFirstName.Focus();
                return;
            }
            if (txtLastName.Text == "") {
                MessageBox.Show("Last Name Required");
                txtLastName.Focus();
                return;
            }
            if (txtRegNo.Text == "") {
                MessageBox.Show("RegNo Required");
                txtRegNo.Focus();
                return;
            }
            if (cboGender.Text == "") {
                MessageBox.Show("Gender Required");
                cboGender.Focus();
                return;
            }
            if (dateTimePicker1.Text == "" /*|| dateTimePicker1.Text == CurDate()*/) {
                MessageBox.Show("Date of Birth Required");
                dateTimePicker1.Focus();
                return;
            }

            String FName, LName, DOB, Gender;
            int RegNO;

            FName = txtFirstName.Text;
            LName = txtLastName.Text;
            DOB = dateTimePicker1.Text;
            Gender = cboGender.Text;
            RegNO = Convert.ToInt32(txtRegNo.Text);//conerting string to 32 bit integer
            //fetching input from the user

            //dgvStudentGrade.Rows.Add(FName, LName, Gender, DOB, RegNO);

            tabControl1.SelectTab(1);
        }

        private void btnPreviousCourse_Click(object sender, EventArgs e)
        {
            tabControl1.SelectTab(0);
        }

        private void btnPreviousAcademic_Click(object sender, EventArgs e)
        {
            tabControl1.SelectTab(1);
        }

        private void txtCat1(object sender, CancelEventArgs e)
        {

        }
    }
}
