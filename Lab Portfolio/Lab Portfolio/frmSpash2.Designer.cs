﻿namespace Lab_Portfolio
{
    partial class frmSpash2
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.pgbStatusSpash = new System.Windows.Forms.ProgressBar();
            this.lblStatusSpash = new System.Windows.Forms.Label();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.SuspendLayout();
            // 
            // pgbStatusSpash
            // 
            this.pgbStatusSpash.Location = new System.Drawing.Point(124, 74);
            this.pgbStatusSpash.Name = "pgbStatusSpash";
            this.pgbStatusSpash.Size = new System.Drawing.Size(212, 23);
            this.pgbStatusSpash.TabIndex = 0;
            // 
            // lblStatusSpash
            // 
            this.lblStatusSpash.AutoSize = true;
            this.lblStatusSpash.Location = new System.Drawing.Point(26, 74);
            this.lblStatusSpash.Name = "lblStatusSpash";
            this.lblStatusSpash.Size = new System.Drawing.Size(35, 13);
            this.lblStatusSpash.TabIndex = 1;
            this.lblStatusSpash.Text = "label1";
            // 
            // timer1
            // 
            this.timer1.Enabled = true;
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // frmSpash2
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(416, 190);
            this.Controls.Add(this.lblStatusSpash);
            this.Controls.Add(this.pgbStatusSpash);
            this.Name = "frmSpash2";
            this.Text = "frmSpash2";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ProgressBar pgbStatusSpash;
        private System.Windows.Forms.Label lblStatusSpash;
        private System.Windows.Forms.Timer timer1;
    }
}