﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Lab_Portfolio
{
    public partial class FrmSplash : Form
    {
        public FrmSplash()
        {
            InitializeComponent();
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            timer1.Start();//starting the timer
            PgbStatus.Increment(+5);//???

            if (PgbStatus.Value < 100)
            {
                lblStatus.Text = "Loading" + PgbStatus.Value + "%";
            }
            else {
                timer1.Stop();//Close splash form
                this.Hide();//Hide splash form

                FrmLogin flogin = new FrmLogin();//creating form object
                flogin.Show();//displaying the login form
            }
        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {

        }
    }
}
