﻿namespace Lab_Portfolio
{
    partial class FrmContainers
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.btnNextPersonalDetails = new System.Windows.Forms.Button();
            this.textBox2 = new System.Windows.Forms.TextBox();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.lblRegNoPersonalDetails = new System.Windows.Forms.Label();
            this.lblNamePersonalDetails = new System.Windows.Forms.Label();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.btnNextContactDetails = new System.Windows.Forms.Button();
            this.btnPreviousContactDetails = new System.Windows.Forms.Button();
            this.textBox4 = new System.Windows.Forms.TextBox();
            this.textBox3 = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.tabPage3 = new System.Windows.Forms.TabPage();
            this.BtnFinish = new System.Windows.Forms.Button();
            this.btnPreviousNextOfKin = new System.Windows.Forms.Button();
            this.textBox6 = new System.Windows.Forms.TextBox();
            this.textBox5 = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.tabControl1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.tabPage2.SuspendLayout();
            this.tabPage3.SuspendLayout();
            this.SuspendLayout();
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Controls.Add(this.tabPage3);
            this.tabControl1.Location = new System.Drawing.Point(27, 33);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(301, 219);
            this.tabControl1.TabIndex = 0;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.btnNextPersonalDetails);
            this.tabPage1.Controls.Add(this.textBox2);
            this.tabPage1.Controls.Add(this.textBox1);
            this.tabPage1.Controls.Add(this.lblRegNoPersonalDetails);
            this.tabPage1.Controls.Add(this.lblNamePersonalDetails);
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(293, 193);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "Personal Details";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // btnNextPersonalDetails
            // 
            this.btnNextPersonalDetails.Location = new System.Drawing.Point(194, 152);
            this.btnNextPersonalDetails.Name = "btnNextPersonalDetails";
            this.btnNextPersonalDetails.Size = new System.Drawing.Size(75, 23);
            this.btnNextPersonalDetails.TabIndex = 4;
            this.btnNextPersonalDetails.Text = "Next";
            this.btnNextPersonalDetails.UseVisualStyleBackColor = true;
            this.btnNextPersonalDetails.Click += new System.EventHandler(this.btnNextPersonalDetails_Click);
            // 
            // textBox2
            // 
            this.textBox2.Location = new System.Drawing.Point(76, 34);
            this.textBox2.Name = "textBox2";
            this.textBox2.Size = new System.Drawing.Size(100, 20);
            this.textBox2.TabIndex = 3;
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(76, 7);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(100, 20);
            this.textBox1.TabIndex = 2;
            // 
            // lblRegNoPersonalDetails
            // 
            this.lblRegNoPersonalDetails.AutoSize = true;
            this.lblRegNoPersonalDetails.Location = new System.Drawing.Point(7, 41);
            this.lblRegNoPersonalDetails.Name = "lblRegNoPersonalDetails";
            this.lblRegNoPersonalDetails.Size = new System.Drawing.Size(41, 13);
            this.lblRegNoPersonalDetails.TabIndex = 1;
            this.lblRegNoPersonalDetails.Text = "RegNo";
            // 
            // lblNamePersonalDetails
            // 
            this.lblNamePersonalDetails.AutoSize = true;
            this.lblNamePersonalDetails.Location = new System.Drawing.Point(7, 7);
            this.lblNamePersonalDetails.Name = "lblNamePersonalDetails";
            this.lblNamePersonalDetails.Size = new System.Drawing.Size(35, 13);
            this.lblNamePersonalDetails.TabIndex = 0;
            this.lblNamePersonalDetails.Text = "Name";
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.btnNextContactDetails);
            this.tabPage2.Controls.Add(this.btnPreviousContactDetails);
            this.tabPage2.Controls.Add(this.textBox4);
            this.tabPage2.Controls.Add(this.textBox3);
            this.tabPage2.Controls.Add(this.label2);
            this.tabPage2.Controls.Add(this.label1);
            this.tabPage2.Location = new System.Drawing.Point(4, 22);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(293, 193);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "Contact Details";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // btnNextContactDetails
            // 
            this.btnNextContactDetails.Location = new System.Drawing.Point(193, 151);
            this.btnNextContactDetails.Name = "btnNextContactDetails";
            this.btnNextContactDetails.Size = new System.Drawing.Size(75, 23);
            this.btnNextContactDetails.TabIndex = 5;
            this.btnNextContactDetails.Text = "Next";
            this.btnNextContactDetails.UseVisualStyleBackColor = true;
            this.btnNextContactDetails.Click += new System.EventHandler(this.btnNextContactDetails_Click);
            // 
            // btnPreviousContactDetails
            // 
            this.btnPreviousContactDetails.Location = new System.Drawing.Point(31, 151);
            this.btnPreviousContactDetails.Name = "btnPreviousContactDetails";
            this.btnPreviousContactDetails.Size = new System.Drawing.Size(75, 23);
            this.btnPreviousContactDetails.TabIndex = 4;
            this.btnPreviousContactDetails.Text = "Previous";
            this.btnPreviousContactDetails.UseVisualStyleBackColor = true;
            this.btnPreviousContactDetails.Click += new System.EventHandler(this.btnPreviousContactDetails_Click);
            // 
            // textBox4
            // 
            this.textBox4.Location = new System.Drawing.Point(69, 58);
            this.textBox4.Name = "textBox4";
            this.textBox4.Size = new System.Drawing.Size(100, 20);
            this.textBox4.TabIndex = 3;
            // 
            // textBox3
            // 
            this.textBox3.Location = new System.Drawing.Point(69, 22);
            this.textBox3.Name = "textBox3";
            this.textBox3.Size = new System.Drawing.Size(100, 20);
            this.textBox3.TabIndex = 2;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(7, 65);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(32, 13);
            this.label2.TabIndex = 1;
            this.label2.Text = "Email";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(7, 29);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(39, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Tel No";
            // 
            // tabPage3
            // 
            this.tabPage3.Controls.Add(this.BtnFinish);
            this.tabPage3.Controls.Add(this.btnPreviousNextOfKin);
            this.tabPage3.Controls.Add(this.textBox6);
            this.tabPage3.Controls.Add(this.textBox5);
            this.tabPage3.Controls.Add(this.label4);
            this.tabPage3.Controls.Add(this.label3);
            this.tabPage3.Location = new System.Drawing.Point(4, 22);
            this.tabPage3.Name = "tabPage3";
            this.tabPage3.Size = new System.Drawing.Size(293, 193);
            this.tabPage3.TabIndex = 2;
            this.tabPage3.Text = "Next of Kin";
            this.tabPage3.UseVisualStyleBackColor = true;
            // 
            // BtnFinish
            // 
            this.BtnFinish.Location = new System.Drawing.Point(179, 149);
            this.BtnFinish.Name = "BtnFinish";
            this.BtnFinish.Size = new System.Drawing.Size(75, 23);
            this.BtnFinish.TabIndex = 5;
            this.BtnFinish.Text = "Finish";
            this.BtnFinish.UseVisualStyleBackColor = true;
            this.BtnFinish.Click += new System.EventHandler(this.btnFinish_Click);
            // 
            // btnPreviousNextOfKin
            // 
            this.btnPreviousNextOfKin.Location = new System.Drawing.Point(21, 149);
            this.btnPreviousNextOfKin.Name = "btnPreviousNextOfKin";
            this.btnPreviousNextOfKin.Size = new System.Drawing.Size(75, 23);
            this.btnPreviousNextOfKin.TabIndex = 4;
            this.btnPreviousNextOfKin.Text = "Previous";
            this.btnPreviousNextOfKin.UseVisualStyleBackColor = true;
            this.btnPreviousNextOfKin.Click += new System.EventHandler(this.btnPreviousNextOfKin_Click);
            // 
            // textBox6
            // 
            this.textBox6.Location = new System.Drawing.Point(85, 54);
            this.textBox6.Name = "textBox6";
            this.textBox6.Size = new System.Drawing.Size(100, 20);
            this.textBox6.TabIndex = 3;
            // 
            // textBox5
            // 
            this.textBox5.Location = new System.Drawing.Point(85, 26);
            this.textBox5.Name = "textBox5";
            this.textBox5.Size = new System.Drawing.Size(100, 20);
            this.textBox5.TabIndex = 2;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(18, 61);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(65, 13);
            this.label4.TabIndex = 1;
            this.label4.Text = "Contact Info";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(18, 33);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(35, 13);
            this.label3.TabIndex = 0;
            this.label3.Text = "Name";
            // 
            // FrmContainers
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(354, 323);
            this.Controls.Add(this.tabControl1);
            this.Name = "FrmContainers";
            this.Text = "FrmContainers";
            this.tabControl1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.tabPage1.PerformLayout();
            this.tabPage2.ResumeLayout(false);
            this.tabPage2.PerformLayout();
            this.tabPage3.ResumeLayout(false);
            this.tabPage3.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.TabPage tabPage3;
        private System.Windows.Forms.Button btnNextPersonalDetails;
        private System.Windows.Forms.TextBox textBox2;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.Label lblRegNoPersonalDetails;
        private System.Windows.Forms.Label lblNamePersonalDetails;
        private System.Windows.Forms.Button btnNextContactDetails;
        private System.Windows.Forms.Button btnPreviousContactDetails;
        private System.Windows.Forms.TextBox textBox4;
        private System.Windows.Forms.TextBox textBox3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button BtnFinish;
        private System.Windows.Forms.Button btnPreviousNextOfKin;
        private System.Windows.Forms.TextBox textBox6;
        private System.Windows.Forms.TextBox textBox5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
    }
}