﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Lab_Portfolio
{
    public partial class frmMeals : Form
    {
        public frmMeals()
        {
            InitializeComponent();

            //populating listbox
            lstMenu.Items.Add("Juice");
            lstMenu.Items.Add("Soda");
            lstMenu.Items.Add("Chapati");
            lstMenu.Items.Add("Mix");
            lstMenu.Items.Add("Pasua");
            lstMenu.Items.Add("Smokie"); 
           
            //labelling the listboxes and counting the items
            lblMenu.Text = "Menu: " + lstMenu.Items.Count;
            lblOrder.Text = "Order: " + lstOrder.Items.Count;

        }

        private void frmMeals_Load(object sender, EventArgs e)
        {

        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            try
            {
                lstOrder.Items.Add(lstMenu.SelectedItem);
                //transferring data from menu to order
                lstMenu.Items.Remove(lstMenu.SelectedItem);
                //removing the selected item from menu

                //labelling the listboxes and counting the items
                lblMenu.Text = "Menu: " + lstMenu.Items.Count;
                lblOrder.Text = "Order: " + lstOrder.Items.Count;
            }
            catch {
                MessageBox.Show("Please select and item to continue");
                return;
                //displaying error message and returning control
            }
        }

        private void btnRemove_Click(object sender, EventArgs e)
        {
            try
            {
                lstMenu.Items.Add(lstOrder.SelectedItem);
                //adding the item to menu listbox
                lstOrder.Items.Remove(lstOrder.SelectedItem);
                //removing the selected item from the order listbox

                //labelling the listboxes and counting the items
                lblMenu.Text = "Menu: " + lstMenu.Items.Count;
                lblOrder.Text = "Order: " + lstOrder.Items.Count;

            }
            catch {
                MessageBox.Show("Please select an item to continue");
                return;
            }
        }
    }
}
