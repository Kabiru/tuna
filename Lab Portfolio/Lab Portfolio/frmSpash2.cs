﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Lab_Portfolio
{
    public partial class frmSpash2 : Form
    {
        public frmSpash2()
        {
            InitializeComponent();
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            //timer1.Enabled = true;//enabling the timer
            timer1.Start();//starting the timer
            pgbStatusSpash.Increment(+5);//incrementing the progress control bar

            if (pgbStatusSpash.Value < 100)
            {
                lblStatusSpash.Text = "Loading.. " + pgbStatusSpash.Value + "%";
            }
            else {
                timer1.Stop();//stopping the timer
                this.Hide();//hiding splash form


                //FrmLogin flogin = new FrmLogin();//creating form object
                //flogin.Show();//displaying the login form
                MDIParent1 par = new MDIParent1();
                //par.MdiParent = this;
                par.Show();//calling the parent form
            }
        }
    }
}
