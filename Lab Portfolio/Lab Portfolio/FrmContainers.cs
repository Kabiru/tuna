﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Lab_Portfolio
{
    public partial class FrmContainers : Form
    {
        public FrmContainers()
        {
            InitializeComponent();
        }

        private void btnNextPersonalDetails_Click(object sender, EventArgs e)
        {
            tabControl1.SelectTab(1);//select next tab
        }

        private void btnNextContactDetails_Click(object sender, EventArgs e)
        {
            tabControl1.SelectTab(2);
        }

        private void btnPreviousContactDetails_Click(object sender, EventArgs e)
        {
            tabControl1.SelectTab(0);
        }

        private void btnPreviousNextOfKin_Click(object sender, EventArgs e)
        {
            tabControl1.SelectTab(1);
        }

        private void btnFinish_Click(object sender, EventArgs e)
        {
           
        }
    }
}
