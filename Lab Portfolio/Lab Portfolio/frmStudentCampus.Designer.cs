﻿namespace Lab_Portfolio
{
    partial class frmStudentCampus
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.btnCancelDetails = new System.Windows.Forms.Button();
            this.btnNextDetails = new System.Windows.Forms.Button();
            this.cboGender = new System.Windows.Forms.ComboBox();
            this.lblGender = new System.Windows.Forms.Label();
            this.dateTimePicker1 = new System.Windows.Forms.DateTimePicker();
            this.txtLastName = new System.Windows.Forms.TextBox();
            this.lblLastName = new System.Windows.Forms.Label();
            this.txtRegNo = new System.Windows.Forms.TextBox();
            this.txtFirstName = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.lblRegNo = new System.Windows.Forms.Label();
            this.lblFirstName = new System.Windows.Forms.Label();
            this.txtcat = new System.Windows.Forms.TabPage();
            this.txtExam = new System.Windows.Forms.TextBox();
            this.lblExam = new System.Windows.Forms.Label();
            this.txtCat2 = new System.Windows.Forms.TextBox();
            this.lblCat2 = new System.Windows.Forms.Label();
            this.lblCat1 = new System.Windows.Forms.Label();
            this.cboUnits = new System.Windows.Forms.ComboBox();
            this.lblUnit = new System.Windows.Forms.Label();
            this.btnExitCourse = new System.Windows.Forms.Button();
            this.btnPreviousCourse = new System.Windows.Forms.Button();
            this.btnNextCourse = new System.Windows.Forms.Button();
            this.rdoYear4 = new System.Windows.Forms.RadioButton();
            this.rdoYear3 = new System.Windows.Forms.RadioButton();
            this.rdoYear2 = new System.Windows.Forms.RadioButton();
            this.rdoYear1 = new System.Windows.Forms.RadioButton();
            this.lblYear = new System.Windows.Forms.Label();
            this.tabPage3 = new System.Windows.Forms.TabPage();
            this.btnPreviousAcademic = new System.Windows.Forms.Button();
            this.btnDone = new System.Windows.Forms.Button();
            this.dgvStudentGrade = new System.Windows.Forms.DataGridView();
            this.Column1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column5 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column6 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.tabControl1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.txtcat.SuspendLayout();
            this.tabPage3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvStudentGrade)).BeginInit();
            this.SuspendLayout();
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.txtcat);
            this.tabControl1.Controls.Add(this.tabPage3);
            this.tabControl1.Location = new System.Drawing.Point(12, 37);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(637, 257);
            this.tabControl1.TabIndex = 0;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.btnCancelDetails);
            this.tabPage1.Controls.Add(this.btnNextDetails);
            this.tabPage1.Controls.Add(this.cboGender);
            this.tabPage1.Controls.Add(this.lblGender);
            this.tabPage1.Controls.Add(this.dateTimePicker1);
            this.tabPage1.Controls.Add(this.txtLastName);
            this.tabPage1.Controls.Add(this.lblLastName);
            this.tabPage1.Controls.Add(this.txtRegNo);
            this.tabPage1.Controls.Add(this.txtFirstName);
            this.tabPage1.Controls.Add(this.label3);
            this.tabPage1.Controls.Add(this.lblRegNo);
            this.tabPage1.Controls.Add(this.lblFirstName);
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(629, 231);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "Personal Details";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // btnCancelDetails
            // 
            this.btnCancelDetails.Location = new System.Drawing.Point(209, 168);
            this.btnCancelDetails.Name = "btnCancelDetails";
            this.btnCancelDetails.Size = new System.Drawing.Size(75, 23);
            this.btnCancelDetails.TabIndex = 14;
            this.btnCancelDetails.Text = "&Cancel";
            this.btnCancelDetails.UseVisualStyleBackColor = true;
            // 
            // btnNextDetails
            // 
            this.btnNextDetails.Location = new System.Drawing.Point(354, 169);
            this.btnNextDetails.Name = "btnNextDetails";
            this.btnNextDetails.Size = new System.Drawing.Size(75, 23);
            this.btnNextDetails.TabIndex = 13;
            this.btnNextDetails.Text = "&Next";
            this.btnNextDetails.UseVisualStyleBackColor = true;
            this.btnNextDetails.Click += new System.EventHandler(this.btnNextDetails_Click);
            // 
            // cboGender
            // 
            this.cboGender.FormattingEnabled = true;
            this.cboGender.Location = new System.Drawing.Point(308, 55);
            this.cboGender.Name = "cboGender";
            this.cboGender.Size = new System.Drawing.Size(121, 21);
            this.cboGender.TabIndex = 12;
            this.cboGender.SelectedIndexChanged += new System.EventHandler(this.cboGender_SelectedIndexChanged);
            // 
            // lblGender
            // 
            this.lblGender.AutoSize = true;
            this.lblGender.Location = new System.Drawing.Point(227, 57);
            this.lblGender.Name = "lblGender";
            this.lblGender.Size = new System.Drawing.Size(42, 13);
            this.lblGender.TabIndex = 11;
            this.lblGender.Text = "Gender";
            // 
            // dateTimePicker1
            // 
            this.dateTimePicker1.Location = new System.Drawing.Point(98, 107);
            this.dateTimePicker1.Name = "dateTimePicker1";
            this.dateTimePicker1.Size = new System.Drawing.Size(187, 20);
            this.dateTimePicker1.TabIndex = 10;
            // 
            // txtLastName
            // 
            this.txtLastName.Location = new System.Drawing.Point(308, 22);
            this.txtLastName.Name = "txtLastName";
            this.txtLastName.Size = new System.Drawing.Size(100, 20);
            this.txtLastName.TabIndex = 9;
            // 
            // lblLastName
            // 
            this.lblLastName.AutoSize = true;
            this.lblLastName.Location = new System.Drawing.Point(227, 22);
            this.lblLastName.Name = "lblLastName";
            this.lblLastName.Size = new System.Drawing.Size(58, 13);
            this.lblLastName.TabIndex = 8;
            this.lblLastName.Text = "Last Name";
            // 
            // txtRegNo
            // 
            this.txtRegNo.Location = new System.Drawing.Point(98, 57);
            this.txtRegNo.Name = "txtRegNo";
            this.txtRegNo.Size = new System.Drawing.Size(100, 20);
            this.txtRegNo.TabIndex = 5;
            // 
            // txtFirstName
            // 
            this.txtFirstName.Location = new System.Drawing.Point(98, 22);
            this.txtFirstName.Name = "txtFirstName";
            this.txtFirstName.Size = new System.Drawing.Size(100, 20);
            this.txtFirstName.TabIndex = 4;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(6, 107);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(66, 13);
            this.label3.TabIndex = 2;
            this.label3.Text = "Date of Birth";
            // 
            // lblRegNo
            // 
            this.lblRegNo.AutoSize = true;
            this.lblRegNo.Location = new System.Drawing.Point(3, 57);
            this.lblRegNo.Name = "lblRegNo";
            this.lblRegNo.Size = new System.Drawing.Size(44, 13);
            this.lblRegNo.TabIndex = 1;
            this.lblRegNo.Text = "Reg No";
            // 
            // lblFirstName
            // 
            this.lblFirstName.AutoSize = true;
            this.lblFirstName.Location = new System.Drawing.Point(3, 22);
            this.lblFirstName.Name = "lblFirstName";
            this.lblFirstName.Size = new System.Drawing.Size(57, 13);
            this.lblFirstName.TabIndex = 0;
            this.lblFirstName.Text = "First Name";
            // 
            // txtcat
            // 
            this.txtcat.Controls.Add(this.textBox1);
            this.txtcat.Controls.Add(this.txtExam);
            this.txtcat.Controls.Add(this.lblExam);
            this.txtcat.Controls.Add(this.txtCat2);
            this.txtcat.Controls.Add(this.lblCat2);
            this.txtcat.Controls.Add(this.lblCat1);
            this.txtcat.Controls.Add(this.cboUnits);
            this.txtcat.Controls.Add(this.lblUnit);
            this.txtcat.Controls.Add(this.btnExitCourse);
            this.txtcat.Controls.Add(this.btnPreviousCourse);
            this.txtcat.Controls.Add(this.btnNextCourse);
            this.txtcat.Controls.Add(this.rdoYear4);
            this.txtcat.Controls.Add(this.rdoYear3);
            this.txtcat.Controls.Add(this.rdoYear2);
            this.txtcat.Controls.Add(this.rdoYear1);
            this.txtcat.Controls.Add(this.lblYear);
            this.txtcat.Location = new System.Drawing.Point(4, 22);
            this.txtcat.Name = "txtcat";
            this.txtcat.Padding = new System.Windows.Forms.Padding(3);
            this.txtcat.Size = new System.Drawing.Size(629, 231);
            this.txtcat.TabIndex = 1;
            this.txtcat.Text = "Course Details";
            this.txtcat.UseVisualStyleBackColor = true;
            this.txtcat.Click += new System.EventHandler(this.tabPage2_Click);
            // 
            // txtExam
            // 
            this.txtExam.Location = new System.Drawing.Point(356, 90);
            this.txtExam.Name = "txtExam";
            this.txtExam.Size = new System.Drawing.Size(44, 20);
            this.txtExam.TabIndex = 15;
            // 
            // lblExam
            // 
            this.lblExam.AutoSize = true;
            this.lblExam.Location = new System.Drawing.Point(296, 90);
            this.lblExam.Name = "lblExam";
            this.lblExam.Size = new System.Drawing.Size(37, 13);
            this.lblExam.TabIndex = 14;
            this.lblExam.Text = "EXAM";
            // 
            // txtCat2
            // 
            this.txtCat2.Location = new System.Drawing.Point(217, 90);
            this.txtCat2.Name = "txtCat2";
            this.txtCat2.Size = new System.Drawing.Size(44, 20);
            this.txtCat2.TabIndex = 13;
            this.txtCat2.TextChanged += new System.EventHandler(this.textBox4_TextChanged_1);
            // 
            // lblCat2
            // 
            this.lblCat2.AutoSize = true;
            this.lblCat2.Location = new System.Drawing.Point(164, 90);
            this.lblCat2.Name = "lblCat2";
            this.lblCat2.Size = new System.Drawing.Size(34, 13);
            this.lblCat2.TabIndex = 12;
            this.lblCat2.Text = "CAT2";
            // 
            // lblCat1
            // 
            this.lblCat1.AutoSize = true;
            this.lblCat1.Location = new System.Drawing.Point(24, 90);
            this.lblCat1.Name = "lblCat1";
            this.lblCat1.Size = new System.Drawing.Size(34, 13);
            this.lblCat1.TabIndex = 10;
            this.lblCat1.Text = "CAT1";
            // 
            // cboUnits
            // 
            this.cboUnits.FormattingEnabled = true;
            this.cboUnits.Location = new System.Drawing.Point(93, 49);
            this.cboUnits.Name = "cboUnits";
            this.cboUnits.Size = new System.Drawing.Size(240, 21);
            this.cboUnits.TabIndex = 9;
            this.cboUnits.SelectedIndexChanged += new System.EventHandler(this.cboUnits_SelectedIndexChanged);
            // 
            // lblUnit
            // 
            this.lblUnit.AutoSize = true;
            this.lblUnit.Location = new System.Drawing.Point(24, 49);
            this.lblUnit.Name = "lblUnit";
            this.lblUnit.Size = new System.Drawing.Size(26, 13);
            this.lblUnit.TabIndex = 8;
            this.lblUnit.Text = "Unit";
            // 
            // btnExitCourse
            // 
            this.btnExitCourse.Location = new System.Drawing.Point(217, 202);
            this.btnExitCourse.Name = "btnExitCourse";
            this.btnExitCourse.Size = new System.Drawing.Size(75, 23);
            this.btnExitCourse.TabIndex = 7;
            this.btnExitCourse.Text = "E&xit";
            this.btnExitCourse.UseVisualStyleBackColor = true;
            this.btnExitCourse.Click += new System.EventHandler(this.btnExitCourse_Click);
            // 
            // btnPreviousCourse
            // 
            this.btnPreviousCourse.Location = new System.Drawing.Point(27, 163);
            this.btnPreviousCourse.Name = "btnPreviousCourse";
            this.btnPreviousCourse.Size = new System.Drawing.Size(75, 23);
            this.btnPreviousCourse.TabIndex = 6;
            this.btnPreviousCourse.Text = "&Previous";
            this.btnPreviousCourse.UseVisualStyleBackColor = true;
            this.btnPreviousCourse.Click += new System.EventHandler(this.btnPreviousCourse_Click);
            // 
            // btnNextCourse
            // 
            this.btnNextCourse.Location = new System.Drawing.Point(373, 163);
            this.btnNextCourse.Name = "btnNextCourse";
            this.btnNextCourse.Size = new System.Drawing.Size(75, 23);
            this.btnNextCourse.TabIndex = 5;
            this.btnNextCourse.Text = "Su&bmit";
            this.btnNextCourse.UseVisualStyleBackColor = true;
            this.btnNextCourse.Click += new System.EventHandler(this.btnNextCourse_Click);
            // 
            // rdoYear4
            // 
            this.rdoYear4.AutoSize = true;
            this.rdoYear4.Location = new System.Drawing.Point(204, 20);
            this.rdoYear4.Name = "rdoYear4";
            this.rdoYear4.Size = new System.Drawing.Size(35, 17);
            this.rdoYear4.TabIndex = 4;
            this.rdoYear4.Text = "IV";
            this.rdoYear4.UseVisualStyleBackColor = true;
            // 
            // rdoYear3
            // 
            this.rdoYear3.AutoSize = true;
            this.rdoYear3.Location = new System.Drawing.Point(164, 20);
            this.rdoYear3.Name = "rdoYear3";
            this.rdoYear3.Size = new System.Drawing.Size(34, 17);
            this.rdoYear3.TabIndex = 3;
            this.rdoYear3.Text = "III";
            this.rdoYear3.UseVisualStyleBackColor = true;
            // 
            // rdoYear2
            // 
            this.rdoYear2.AutoSize = true;
            this.rdoYear2.Location = new System.Drawing.Point(127, 20);
            this.rdoYear2.Name = "rdoYear2";
            this.rdoYear2.Size = new System.Drawing.Size(31, 17);
            this.rdoYear2.TabIndex = 2;
            this.rdoYear2.Text = "II";
            this.rdoYear2.UseVisualStyleBackColor = true;
            // 
            // rdoYear1
            // 
            this.rdoYear1.AutoSize = true;
            this.rdoYear1.Checked = true;
            this.rdoYear1.Location = new System.Drawing.Point(93, 20);
            this.rdoYear1.Name = "rdoYear1";
            this.rdoYear1.Size = new System.Drawing.Size(28, 17);
            this.rdoYear1.TabIndex = 1;
            this.rdoYear1.TabStop = true;
            this.rdoYear1.Text = "I";
            this.rdoYear1.UseVisualStyleBackColor = true;
            // 
            // lblYear
            // 
            this.lblYear.AutoSize = true;
            this.lblYear.Location = new System.Drawing.Point(24, 24);
            this.lblYear.Name = "lblYear";
            this.lblYear.Size = new System.Drawing.Size(29, 13);
            this.lblYear.TabIndex = 0;
            this.lblYear.Text = "Year";
            // 
            // tabPage3
            // 
            this.tabPage3.Controls.Add(this.btnPreviousAcademic);
            this.tabPage3.Controls.Add(this.btnDone);
            this.tabPage3.Controls.Add(this.dgvStudentGrade);
            this.tabPage3.Location = new System.Drawing.Point(4, 22);
            this.tabPage3.Name = "tabPage3";
            this.tabPage3.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage3.Size = new System.Drawing.Size(629, 231);
            this.tabPage3.TabIndex = 2;
            this.tabPage3.Text = "Grade";
            this.tabPage3.UseVisualStyleBackColor = true;
            // 
            // btnPreviousAcademic
            // 
            this.btnPreviousAcademic.Location = new System.Drawing.Point(415, 191);
            this.btnPreviousAcademic.Name = "btnPreviousAcademic";
            this.btnPreviousAcademic.Size = new System.Drawing.Size(75, 23);
            this.btnPreviousAcademic.TabIndex = 9;
            this.btnPreviousAcademic.Text = "&Previous";
            this.btnPreviousAcademic.UseVisualStyleBackColor = true;
            this.btnPreviousAcademic.Click += new System.EventHandler(this.btnPreviousAcademic_Click);
            // 
            // btnDone
            // 
            this.btnDone.Location = new System.Drawing.Point(538, 191);
            this.btnDone.Name = "btnDone";
            this.btnDone.Size = new System.Drawing.Size(75, 23);
            this.btnDone.TabIndex = 8;
            this.btnDone.Text = "&Finish";
            this.btnDone.UseVisualStyleBackColor = true;
            // 
            // dgvStudentGrade
            // 
            this.dgvStudentGrade.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvStudentGrade.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Column1,
            this.Column2,
            this.Column4,
            this.Column5,
            this.Column3,
            this.Column6});
            this.dgvStudentGrade.Location = new System.Drawing.Point(3, 6);
            this.dgvStudentGrade.Name = "dgvStudentGrade";
            this.dgvStudentGrade.Size = new System.Drawing.Size(620, 179);
            this.dgvStudentGrade.TabIndex = 0;
            // 
            // Column1
            // 
            this.Column1.HeaderText = "First Name";
            this.Column1.Name = "Column1";
            this.Column1.ReadOnly = true;
            // 
            // Column2
            // 
            this.Column2.HeaderText = "Last Name";
            this.Column2.Name = "Column2";
            this.Column2.ReadOnly = true;
            // 
            // Column4
            // 
            this.Column4.HeaderText = "Gender";
            this.Column4.Name = "Column4";
            this.Column4.ReadOnly = true;
            // 
            // Column5
            // 
            this.Column5.HeaderText = "Date of Birth";
            this.Column5.Name = "Column5";
            this.Column5.ReadOnly = true;
            // 
            // Column3
            // 
            this.Column3.HeaderText = "Reg No";
            this.Column3.Name = "Column3";
            this.Column3.ReadOnly = true;
            // 
            // Column6
            // 
            this.Column6.HeaderText = "Grade";
            this.Column6.Name = "Column6";
            this.Column6.ReadOnly = true;
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(93, 89);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(44, 20);
            this.textBox1.TabIndex = 16;
            // 
            // frmStudentCampus
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(661, 361);
            this.Controls.Add(this.tabControl1);
            this.Name = "frmStudentCampus";
            this.Text = "frmStudentCampus";
            this.tabControl1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.tabPage1.PerformLayout();
            this.txtcat.ResumeLayout(false);
            this.txtcat.PerformLayout();
            this.tabPage3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvStudentGrade)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.TextBox txtRegNo;
        private System.Windows.Forms.TextBox txtFirstName;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label lblRegNo;
        private System.Windows.Forms.Label lblFirstName;
        private System.Windows.Forms.TabPage txtcat;
        private System.Windows.Forms.TextBox txtLastName;
        private System.Windows.Forms.Label lblLastName;
        private System.Windows.Forms.Button btnCancelDetails;
        private System.Windows.Forms.Button btnNextDetails;
        private System.Windows.Forms.ComboBox cboGender;
        private System.Windows.Forms.Label lblGender;
        private System.Windows.Forms.DateTimePicker dateTimePicker1;
        private System.Windows.Forms.RadioButton rdoYear4;
        private System.Windows.Forms.RadioButton rdoYear3;
        private System.Windows.Forms.RadioButton rdoYear2;
        private System.Windows.Forms.RadioButton rdoYear1;
        private System.Windows.Forms.Label lblYear;
        private System.Windows.Forms.TextBox txtExam;
        private System.Windows.Forms.Label lblExam;
        private System.Windows.Forms.TextBox txtCat2;
        private System.Windows.Forms.Label lblCat2;
       // private System.Windows.Forms.TextBox txtCat1;
        private System.Windows.Forms.Label lblCat1;
        private System.Windows.Forms.ComboBox cboUnits;
        private System.Windows.Forms.Label lblUnit;
        private System.Windows.Forms.Button btnExitCourse;
        private System.Windows.Forms.Button btnPreviousCourse;
        private System.Windows.Forms.Button btnNextCourse;
        private System.Windows.Forms.TabPage tabPage3;
        private System.Windows.Forms.DataGridView dgvStudentGrade;
        private System.Windows.Forms.Button btnPreviousAcademic;
        private System.Windows.Forms.Button btnDone;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column1;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column2;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column4;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column5;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column3;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column6;
        private System.Windows.Forms.TextBox textBox1;

    }
}