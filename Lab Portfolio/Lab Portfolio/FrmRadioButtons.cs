﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Lab_Portfolio
{
    public partial class FrmRadioButtons : Form
    {
        public FrmRadioButtons()
        {
            InitializeComponent();
        }

        private void RdoCash_CheckedChanged(object sender, EventArgs e)
        {
            if (RdoCash.Checked)
                lblPayment.Text = "Enter Amount: ";//changes when radio button is checked
        }

        private void RdoMpesa_CheckedChanged(object sender, EventArgs e)
        {
            if (RdoMpesa.Checked)
                lblPayment.Text = "Enter Mpesa Code: ";
        }

        private void RdoCheque_CheckedChanged(object sender, EventArgs e)
        {
            if (RdoCheque.Checked)
                lblPayment.Text = "Enter Cheque Number";
        }

        private void FrmRadioButtons_Load(object sender, EventArgs e)
        {

        }
    }
}
