﻿namespace Lab_Portfolio
{
    partial class FrmRadioButtons
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.RdoCash = new System.Windows.Forms.RadioButton();
            this.RdoMpesa = new System.Windows.Forms.RadioButton();
            this.RdoCheque = new System.Windows.Forms.RadioButton();
            this.lblPayment = new System.Windows.Forms.Label();
            this.txtPayment = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // RdoCash
            // 
            this.RdoCash.AutoSize = true;
            this.RdoCash.Location = new System.Drawing.Point(35, 36);
            this.RdoCash.Name = "RdoCash";
            this.RdoCash.Size = new System.Drawing.Size(49, 17);
            this.RdoCash.TabIndex = 0;
            this.RdoCash.TabStop = true;
            this.RdoCash.Text = "Cash";
            this.RdoCash.UseVisualStyleBackColor = true;
            this.RdoCash.CheckedChanged += new System.EventHandler(this.RdoCash_CheckedChanged);
            // 
            // RdoMpesa
            // 
            this.RdoMpesa.AutoSize = true;
            this.RdoMpesa.Location = new System.Drawing.Point(35, 69);
            this.RdoMpesa.Name = "RdoMpesa";
            this.RdoMpesa.Size = new System.Drawing.Size(57, 17);
            this.RdoMpesa.TabIndex = 1;
            this.RdoMpesa.TabStop = true;
            this.RdoMpesa.Text = "Mpesa";
            this.RdoMpesa.UseVisualStyleBackColor = true;
            this.RdoMpesa.CheckedChanged += new System.EventHandler(this.RdoMpesa_CheckedChanged);
            // 
            // RdoCheque
            // 
            this.RdoCheque.AutoSize = true;
            this.RdoCheque.Location = new System.Drawing.Point(35, 106);
            this.RdoCheque.Name = "RdoCheque";
            this.RdoCheque.Size = new System.Drawing.Size(62, 17);
            this.RdoCheque.TabIndex = 2;
            this.RdoCheque.TabStop = true;
            this.RdoCheque.Text = "Cheque";
            this.RdoCheque.UseVisualStyleBackColor = true;
            // 
            // lblPayment
            // 
            this.lblPayment.AutoSize = true;
            this.lblPayment.Location = new System.Drawing.Point(32, 152);
            this.lblPayment.Name = "lblPayment";
            this.lblPayment.Size = new System.Drawing.Size(35, 13);
            this.lblPayment.TabIndex = 3;
            this.lblPayment.Text = "label1";
            // 
            // txtPayment
            // 
            this.txtPayment.Location = new System.Drawing.Point(152, 145);
            this.txtPayment.Name = "txtPayment";
            this.txtPayment.Size = new System.Drawing.Size(100, 20);
            this.txtPayment.TabIndex = 4;
            // 
            // FrmRadioButtons
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(284, 261);
            this.Controls.Add(this.txtPayment);
            this.Controls.Add(this.lblPayment);
            this.Controls.Add(this.RdoCheque);
            this.Controls.Add(this.RdoMpesa);
            this.Controls.Add(this.RdoCash);
            this.Name = "FrmRadioButtons";
            this.Text = "FrmRadioButtons";
            this.Load += new System.EventHandler(this.FrmRadioButtons_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.RadioButton RdoCash;
        private System.Windows.Forms.RadioButton RdoMpesa;
        private System.Windows.Forms.RadioButton RdoCheque;
        private System.Windows.Forms.Label lblPayment;
        private System.Windows.Forms.TextBox txtPayment;
    }
}