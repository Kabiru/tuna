﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Lab_Portfolio
{
    public partial class FrmStudent : Form
    {
        public FrmStudent()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (TxtRegNo.Text == "")
            {
                MessageBox.Show("Reg No required");
                TxtRegNo.Focus();//focus cursor to regno textbox
                return;
            }
            if (txtDob.Text == "")
            {
                MessageBox.Show("Date of Birth Required");
                txtDob.Focus();//focus cursor to dob textbox
            }
            if (txtGender.Text == "")
            {
                MessageBox.Show("Gender Required");
                txtGender.Focus();//focus cursor to gender textbox
            }
            if (txtName.Text == "")
            {
                MessageBox.Show("Name Required");
                txtName.Focus();//focus cursor to name textbox
            }
            if (txtPostalAddress.Text == "")
            {
                MessageBox.Show("Postal Address required");
                txtPostalAddress.Focus();//focus cursor to postal address textbox
            }


            String stdName, stdGender, stdPostalAddress, stdDateOfBirth;
            int stdRegNo;

            stdName = txtName.Text;
            stdPostalAddress = txtPostalAddress.Text;
            stdRegNo = Convert.ToInt32(TxtRegNo.Text);
            stdDateOfBirth = txtDob.Text;
            stdGender = txtGender.Text;//fetching input from the textbox

            dgvStudent.Rows.Add(stdName, stdRegNo, stdDateOfBirth, stdGender, stdPostalAddress);
            //enabling addition to data gridview


        }

        private void dgvStudent_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void button2_Click(object sender, EventArgs e)
        {
            //clearing the textboxes
            txtName.Clear();
            TxtRegNo.Clear();
            txtDob.Clear();
            txtGender.Clear();
            txtPostalAddress.Clear();
        }
    }
}
